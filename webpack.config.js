var webpack = require('webpack');
var path = require('path');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin');

var BUILD_DIR = path.resolve(__dirname, 'src/resources/data/www/');

var config = {
    devServer: {
        publicPath: "/", 
        stats: "minimal",
        proxy: [
            {
                // Make sure that we get a redirect if not logged in.
                context: function (pathname, req) {
                    var proxy = false;
                    proxy |= pathname.startsWith("/activate");
                    proxy |= pathname.startsWith("/clearall");
                    proxy |= pathname.startsWith("/configuration");
                    proxy |= pathname.startsWith("/deactivateAll");
                    proxy |= pathname.startsWith("/index");
                    proxy |= pathname.startsWith("/program");
                    proxy |= pathname.startsWith("/programs");
                    proxy |= pathname.startsWith("/toggle");
                    proxy |= pathname.startsWith("/scanner");
                    return proxy;
                },
                target: "http://192.168.4.1"
            }
        ]
    },
    entry: 'index.js',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: "css-loader",
                            query: {
                                sourceMap: true
                            }
                        }
                    ],
                    fallback: "style-loader"
                })
            }/*,
            {
                test: /\.(svg|woff|woff2|ttf|eot)$/,
                loader: "file-loader?&outputPath=/fonts/",
                options: {
                    name: function (value) {
                        var lastPart = value.replace(/^.+[/\\]([^/\\]+)$/, "$1").replace(/^(.{3}).+[.]([^.]+)$/, "/fonts/$1.$2");
                        console.log("Name is ", value, lastPart);
                        return lastPart;
                    }
                }
            }*/,
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                loader: "file-loader?name=images/[name].[ext]",
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "css/app.css",
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            template: "src/main/js/public/index.html",
            inject: "body",
            filename: "index.html"
        }), new HtmlWebpackExternalsPlugin({
            externals: [
              {
                module: 'bootstrap',
                entry: {
                    path: 'dist/css/bootstrap.min.css',
                    cwpPatternConfig: {
                        transformPath(targetPath, sourcePath) {
                            var result = targetPath.replace(/^.+[/\\]([^/\\]+)$/, "$1").replace(/^(.+)[.]([^.]+)$/, "css/$1.$2");
                            return result;
                        }
                    }
                },
                supplements: [{
                    path: 'dist/fonts/', 
                    cwpPatternConfig: {
                        transformPath(targetPath, sourcePath) {
                            var result = targetPath.replace(/^.+[/\\]([^/\\]+)$/, "$1").replace(/^(.{3}).+[.]([^.]+)$/, "fonts/$1.$2");
                            return result;
                        }
                    },
                }],
              }
            ],
            outputPath: '.',
        })/*,
        new CopyWebpackPlugin([
            { from: 'css/bootstrap.min.css', to: 'css/' },
            { from: 'fonts/*', to: './' }
        ], {context: './node_modules/bootstrap/dist/'})*/
    ],
    resolve: {
        extensions: ['.js', '.jsx', '.css'],
        modules: ["./node_modules", "./src/main/js/src"]
    }
};

module.exports = config;
