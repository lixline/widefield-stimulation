#!/opt/local/bin/python3.6
import re
import urllib3

http = urllib3.PoolManager()
indexRequest = http.request('GET', 'http://192.168.4.1/programs', preload_content=False)

with open("./data/index", 'wb') as index:
    for line in indexRequest.read(8192).splitlines():
        index.write(line)
        index.write('\n'.encode('utf-8'))
        lineText = line.decode('utf-8')
        result = re.match("^\[(\d+)\]$", lineText)
        if result:
            id = result.group(1);
            print("Writing "+id+".lig..."); 
            programRequest = http.request('GET', 'http://192.168.4.1/program?id='+id, preload_content=False)
            with open("./data/"+id+".lig", 'wb') as program:
                for line in programRequest.read(8192).splitlines():
                    if (re.match("^-------$", line.decode('utf-8'))):
                        break;
                    program.write(line)
                    program.write('\n'.encode('utf-8'))

            programRequest.release_conn()

indexRequest.release_conn()

configPropertiesRequest = http.request('GET', 'http://192.168.4.1/configProperties', preload_content=False)
with open("./data/config.properties", 'wb') as configProperties:
    print("Writing config.properties..."); 
    for line in configPropertiesRequest.read(8192).splitlines():
        configProperties.write(line)
        configProperties.write('\n'.encode('utf-8'))

configPropertiesRequest.release_conn()
