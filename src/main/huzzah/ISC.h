#ifndef ISC_h
#define ISC_h

//#include <Arduino.h>

class ISC {
  private:
    int cyclesPerMicro = NULL;
    uint32_t lastRolloverMicros = NULL;
    static inline uint32_t getCycleCount();
    void measureCyclesPerMicro();
    void adjustLastRollover();
  public:
    void loop();
    boolean isReady();
    uint32_t getInterruptSafeTime();
};
#endif

