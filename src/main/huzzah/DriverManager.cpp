#include <ArduinoJson.h>
#include "DriverManager.h"
#include "MCP4922.h"
#include "Neopixel.h"

DriverManager::DriverManager() {
  this->characteristics = new Characteristic*[4];
  this->characteristics[this->characteristicCount++] = new Linear();
  //this->characteristics[this->characteristicCount++] = new Roither_364nm_RLT360_BL_TO18();
  //this->characteristics[this->characteristicCount++] = new Roither_419nm_VL415_5_15();
  //this->characteristics[this->characteristicCount++] = new Roither_469nm_RLS_B465();
  //this->characteristics[this->characteristicCount++] = new Roither_486nm_RLS_5B_475_S();
  //this->characteristics[this->characteristicCount++] = new Roither_576nm_B5B_433_20();

  this->characteristics[this->characteristicCount++] = new LED590_33W_AB_593nm();
  this->characteristics[this->characteristicCount++] = new RLS_5B_475_S_AB_484nm();
  this->characteristics[this->characteristicCount++] = new VL415_5_15_A_68_Ohm_420nm();
  this->characteristics[this->characteristicCount++] = new VL415_5_15_B_22_Ohm_420nm();
  this->characteristics[this->characteristicCount++] = new RLT_360_15_30_3CC_15_Ohm_363nm();


  // Adding here? Don't forget to adjust the overall length of the pointer array just above!
};

void DriverManager::registerDriverConfig(String driverConfigName, String driverConfig)
{ 
  Serial.printf("Driver config name '%s' found with config '%s'.\n", driverConfigName.c_str(), driverConfig.c_str());

  Driver* newDriver = NULL;
  char* parts = const_cast<char*>(strtok(const_cast<char*>(driverConfig.c_str()), ";"));
  if (parts != NULL) {
    const char* driver = const_cast<char*>(parts);
    if (strcmp(driver, "neopixel") == 0) {
        long pin = strtol(strtok(NULL, ";"), NULL, 10);
        long count = strtol(strtok(NULL, ";"), NULL, 10);
        newDriver = new Neopixel(driverConfigName, pin, count);
    } else if (strcmp(driver, "mcp4922") == 0) {
        long csPin = strtol(strtok(NULL, ";"), NULL, 10);
        newDriver = new MCP4922(driverConfigName, csPin);
    } else {
      Serial.printf("Unknown driver alias '%s' found, ignoring.\n", driver);
    }
  }

  if (drivers == NULL) {
      drivers = new Driver*[1];
      drivers[0] = newDriver;
  } else {
      Driver** newDrivers = new Driver*[1+driverCount];
      memcpy(newDrivers, drivers, driverCount*sizeof(drivers[0]));
      newDrivers[driverCount] = newDriver;
      drivers = newDrivers;
  }
  driverCount++;
}

void DriverManager::addDriverConfig(JsonObject& root) {
    JsonArray& added = root.createNestedArray("drivers");
    for (int i = 0; i < driverCount; i++) {
        Driver* driver = drivers[i];
        JsonObject& obj = added.createNestedObject();
        obj["alias"] = driver->getName();
        obj["type"] = driver->getType();
        obj["addressCount"] = driver->getAddressCount();
        obj["channelCount"] = driver->getChannelCount();
    }
    JsonArray& jsonCharacteristic = root.createNestedArray("characteristics");
    for (int i = 0; i < characteristicCount; i++) {
        Characteristic* characteristic = characteristics[i];
        jsonCharacteristic.add(characteristic->getName());
    }   
}

void DriverManager::addActiveLEDs(JsonObject& root) {
  JsonArray& leds = root.createNestedArray("leds");
  for (int i = 0; i < driverCount; i++) {
    for (int j = 0; j < drivers[i]->getAddressCount(); j++) {
      for (int k = 0; k < drivers[i]->getChannelCount(); k++) {
        if (drivers[i]->getIntensity(j, k) > 0) {
          char* ledId = new char[12];
          sprintf(ledId, "%i:%i:%i", i, j, k);
          leds.add(ledId);
        }
      }
    }
  }
}

Driver* DriverManager::getDriver(int i) {
    if (i >= 0 && i < driverCount) {
        return drivers[i];
    } else {
        return NULL;
    }
}

void DriverManager::clearAllPixels() {
  if (drivers != NULL) {
    for (int i = 0; i < driverCount; i++) {
      Driver* driver = drivers[i];
      for (int address = 0; address < driver->getAddressCount(); address++) {
        for (int channel = 0; channel < driver->getChannelCount(); channel++) {
          driver->setIntensity(address, channel, 0);
        }
      }
      driver->show();
    }
  }
}

void DriverManager::show() {
  for (int i = 0; i < driverCount; i++) {
    drivers[i]->show();
  }
}

Characteristic* DriverManager::findCharacteristic(String name) {
  for (int i = 0; i < characteristicCount; i++) {
    if (name.equals(characteristics[i]->getName())) {
      Serial.printf("Found characteristics named '%s' at position %i.\n", name.c_str(), i);
      return characteristics[i];
    }
  }

  Serial.printf("Cannot find characteristics named '%s', falling back to '%s'.\n", name.c_str(), characteristics[0]->getName().c_str());
  return characteristics[0]; 
}

Driver::Driver(String name) {
  this->name = name;
}

String Driver::getName() {
  return this->name;
}

Characteristic::Characteristic(String name) {
  this->name = name;
}

String Characteristic::getName() {
  return this->name;
}

Linear::Linear() : Characteristic("Linear") {
}

uint16_t Linear::convertIntensity(uint16_t intensity, uint16_t maxIntensity) {
  uint16_t effectiveIntensity = float(intensity)*maxIntensity/65535;
//  Serial.printf("Linear: Target %f effective %u\n", float(intensity), effectiveIntensity);
  return effectiveIntensity;
}

LED590_33W_AB_593nm::LED590_33W_AB_593nm() : Characteristic("Roithner > (593nm) LED590-33W-AB") {
}

uint16_t LED590_33W_AB_593nm::convertIntensity(uint16_t intensity, uint16_t maxIntensity) {
  float targetIntensity = (intensity == 0) ? 24000 : 29000+(23000*intensity/65535);
  uint16_t effectiveIntensity = targetIntensity*maxIntensity/65535;
//Serial.printf("Roither_469nm_RLS_B465: Target %f effective %u\n", targetIntensity, effectiveIntensity);
  return effectiveIntensity;
}

RLS_5B_475_S_AB_484nm::RLS_5B_475_S_AB_484nm() : Characteristic("Roithner > (484nm) RLS_5B_475_S_AB") {
}

uint16_t RLS_5B_475_S_AB_484nm::convertIntensity(uint16_t intensity, uint16_t maxIntensity) {
  float targetIntensity = (intensity == 0) ? 36000 : 42500+(23000*intensity/65535);
  uint16_t effectiveIntensity = targetIntensity*maxIntensity/65535;
//Serial.printf("Target %f effective %u\n", targetIntensity, effectiveIntensity);
  return effectiveIntensity;
}

VL415_5_15_A_68_Ohm_420nm::VL415_5_15_A_68_Ohm_420nm() : Characteristic("Roithner > (420 nm) VL415_5_15_A") {
}

uint16_t VL415_5_15_A_68_Ohm_420nm::convertIntensity(uint16_t intensity, uint16_t maxIntensity) {
    float targetIntensity = (intensity == 0) ? 41000 : 48500+(16500*intensity/65535);
  uint16_t effectiveIntensity = targetIntensity*maxIntensity/65535;
//Serial.printf("Target %f effective %u\n", targetIntensity, effectiveIntensity);
  return effectiveIntensity;
}

VL415_5_15_B_22_Ohm_420nm::VL415_5_15_B_22_Ohm_420nm() : Characteristic("Roithner > (420 nm) VL415_5_15_B-bright") {
}

uint16_t VL415_5_15_B_22_Ohm_420nm::convertIntensity(uint16_t intensity, uint16_t maxIntensity) {
  float targetIntensity = (intensity == 0) ? 39500 : 47000+(12300*intensity/65535);
  uint16_t effectiveIntensity = targetIntensity*maxIntensity/65535;
//Serial.printf("Target %f effective %u\n", targetIntensity, effectiveIntensity);
  return effectiveIntensity;
}

RLT_360_15_30_3CC_15_Ohm_363nm::RLT_360_15_30_3CC_15_Ohm_363nm() : Characteristic("Roithner > (363 nm) RLT_360_15_30_3CC_15") {
}

uint16_t RLT_360_15_30_3CC_15_Ohm_363nm::convertIntensity(uint16_t intensity, uint16_t maxIntensity)  {
  float targetIntensity = (intensity == 0) ? 39300 : 49300+(11500*intensity/65535);
  uint16_t effectiveIntensity = targetIntensity*maxIntensity/65535;
//Serial.printf("Target %f effective %u\n", targetIntensity, effectiveIntensity);
  return effectiveIntensity;
}
