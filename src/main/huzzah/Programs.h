#ifndef Programs_h
#define Programs_h

#include "Arduino.h"
#include "Lighting.h"

class Programs {
  private:
    int active = 0;
    Lighting* lighting;
    String readFile(String fileName);
    bool writeFile(String fileName, String content);
    String getIndexEntry(int id);
    int getNextProgramId();
    String getUpdatedIndex(int id, String newIndexEntry);
    
  public:
    Programs(Lighting* lighting);
    bool activate(int id);
    void deactivate();
    bool create(String content);
    bool write(int id, String content);
    String read(int id);
    String getIndex();
    String getConfigProperties();
    int getActive();
};

#endif
