#include <FS.h>
#include <SPIFFS.h>
#include "WebServ.h"

Webserver::Webserver(Configuration* configuration, DriverManager* driverManager, Programs* programs, Lighting* lighting)
{
  this->configuration = configuration;
  this->driverManager = driverManager;
  this->programs = programs;
  this->lighting = lighting;
}

void Webserver::setup() {
  server->serveStatic("/", SPIFFS, "/www/").setDefaultFile("index.html");
  server->serveStatic("/fonts/glyphicons-halflings-regular.woff", SPIFFS, "/www/fonts/gly.woff");
  server->serveStatic("/fonts/glyphicons-halflings-regular.woff2", SPIFFS, "/www/fonts/gly.woff2");
  server->serveStatic("/fonts/glyphicons-halflings-regular.ttf", SPIFFS, "/www/fonts/gly.ttf");

  server->on("/", HTTP_GET, std::bind(&Webserver::handleRoot, this, std::placeholders::_1));
  server->on("/configuration", HTTP_GET, std::bind(&Webserver::handleReadConfiguration, this, std::placeholders::_1));
  server->on("/programs", HTTP_GET, std::bind(&Webserver::handleReadIndex, this, std::placeholders::_1));
  server->on("/configProperties", HTTP_GET, std::bind(&Webserver::handleReadConfigProperties, this, std::placeholders::_1));
  server->on("/program", HTTP_POST, std::bind(&Webserver::handleCreateProgram, this, std::placeholders::_1));
  server->on("/program", HTTP_PUT, std::bind(&Webserver::handleWriteProgram, this, std::placeholders::_1));
  server->on("/program", HTTP_GET, std::bind(&Webserver::handleReadProgram, this, std::placeholders::_1));
  server->on("/activate", HTTP_GET, std::bind(&Webserver::handleActivateProgram, this, std::placeholders::_1));
  server->on("/toggle", HTTP_GET, std::bind(&Webserver::handleToggle, this, std::placeholders::_1));   
  server->on("/scanner", HTTP_GET, std::bind(&Webserver::handleGetScanner, this, std::placeholders::_1));
  server->on("/scanner", HTTP_DELETE, std::bind(&Webserver::handleClearScanner, this, std::placeholders::_1));
  server->on("/clearall", HTTP_POST, std::bind(&Webserver::handleClearAll, this, std::placeholders::_1));

  server->begin();
}

void Webserver::handleRoot(AsyncWebServerRequest* request) {
  request->redirect("/index.html");
}

void Webserver::handleReadIndex(AsyncWebServerRequest* request) {
  request->send(200, "text/plain", programs->getIndex());
}
  
void Webserver::handleReadConfigProperties(AsyncWebServerRequest* request) {
  request->send(200, "text/plain", programs->getConfigProperties());
}

void Webserver::handleReadProgram(AsyncWebServerRequest* request) {
  if (!request->hasParam("id")) {
    request->send(400, "text/plain", "id parameter missing or empty.");
  }

  int id = request->getParam("id")->value().toInt();
  request->send(200, "text/plain", programs->read(id));
}

void Webserver::handleCreateProgram(AsyncWebServerRequest* request) {
  if (!request->hasParam("content", true)) {
    request->send(400, "text/plain", "content parameter missing or empty.");
  }
  
  String content = request->getParam("content", true)->value();
  
  if (programs->create(content))
  {
    request->send(200, "text/plain", "program successfully written.");
  }
  else
  {
    request->send(500, "text/plain", "program could not be written.");
  }
}

void Webserver::handleWriteProgram(AsyncWebServerRequest* request) {
  if (!request->hasParam("id", true) || !request->hasParam("content", true)) {
    request->send(400, "text/plain", "id or content parameter missing or empty.");
  }

  int id = request->getParam("id", true)->value().toInt();
  String content = request->getParam("content", true)->value();

  if (programs->write(id, content))
  {
    request->send(200, "text/plain", "program successfully written.");
  }
  else
  {
    request->send(500, "text/plain", "program could not be written.");
  }
}

void Webserver::handleActivateProgram(AsyncWebServerRequest* request) {
  if (!request->hasParam("id")) {
    request->send(400, "text/plain", "id parameter missing or empty.");
  }
  
  int id = request->getParam("id")->value().toInt();

  if (programs->activate(id)) {
    request->send(200, "text/plain", "Program activated.");
  } else {
    request->send(200, "text/plain", "Program cannot be activated.");
  }
}

void Webserver::handleReadConfiguration(AsyncWebServerRequest* request)
{
  DynamicJsonBuffer* jsonBuffer = new DynamicJsonBuffer(512);
  JsonObject& root = jsonBuffer->createObject();
  root["active"] = String(programs->getActive());
  root["wifiPower"] = this->configuration->getWifiPowerAdjustable() ? String(this->configuration->getWifiPower()) : "max";
  driverManager->addDriverConfig(root);
  if (programs->getActive() == -1) {
    driverManager->addActiveLEDs(root);
  }
  String json;
  root.prettyPrintTo(json);
  request->send(200, "application/json", json);
}

void Webserver::handleClearAll(AsyncWebServerRequest *request)
{
  programs->deactivate();
  request->send(200, "text/plain", "Cleared all.");
}

void Webserver::handleGetScanner(AsyncWebServerRequest* request) {
  DynamicJsonBuffer* jsonBuffer = new DynamicJsonBuffer(512);
  JsonObject& root = jsonBuffer->createObject();
  lighting->addScannerData(root);
  String json;
  root.prettyPrintTo(json);
  request->send(200, "application/json", json);
}

void Webserver::handleClearScanner(AsyncWebServerRequest* request) {
  lighting->clearScannerData();
  request->send(200, "text/plain", "Cleared scanner statistics.");
}

void Webserver::handleToggle(AsyncWebServerRequest *request)
{
  if (!request->hasParam("driver") || !request->hasParam("address") || !request->hasParam("channel")) {
    request->send(400, "text/plain", "driver parameter missing or empty.");
    return;
  }

  // Before we start toggling LEDs, we must deactivate the running program.
  if (programs->getActive() != -1) {
    programs->deactivate();
  }

  int driver = request->getParam("driver")->value().toInt();
  int address = request->getParam("address")->value().toInt();
  int channel = request->getParam("channel")->value().toInt();
  driverManager->getDriver(driver)->toggle(address, channel);
  driverManager->getDriver(driver)->show();
  request->send(200, "text/plain", "toggled.");
} 
