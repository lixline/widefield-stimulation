#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include "DriverManager.h"
#include "Lighting.h"
//#include <FunctionalInterrupt.h>
#include <functional> 
#include "inttypes.h"
#include <algorithm> 

#ifdef ESP8266
extern "C" {
#include "user_interface.h"
}
#endif

volatile Lighting* Lighting::singletonInstance;


Lighting::Lighting(DriverManager* driverManager, int disablePin, int scannerPin, ISC* isc)
{
  this->driverManager = driverManager;
  this->disablePin = disablePin;
  this->scannerPin = scannerPin;
  this->isc = isc;

  Lighting::singletonInstance = this;

//  attachInterrupt(digitalPinToInterrupt(scannerPin), Lighting::handleScannerInterrupt, CHANGE);
}

inline uint32_t Lighting::readRNGDataReg()
{
  /* The documentation specifies the random number generator at the
   * following address, which is at odds with the SDK, that specifies
   * it at 0x60035144.  The fact that they're 0x200c0000 bytes apart
   * (lower 16 bits are the same) suggests this might be the same
   * register, just mirrored somewhere else in the address space.
   * Confirmation is required.
   */
  volatile uint32_t *rng_data_reg = (uint32_t *)0x3ff75144;

  /* Read just once.  This is not optimal as the generator has
   * limited throughput due to scarce sources of entropy, specially
   * with the radios turned off.  Might want to revisit this.
   */
  return *rng_data_reg;
}

/*void IRAM_ATTR Lighting::handleScannerInterrupt() {
  if (!Lighting::singletonInstance->isc->isReady()) {
    return;
  }
  
  uint32_t nowMicros = Lighting::singletonInstance->isc->getInterruptSafeTime();
  int scannerState = digitalRead(Lighting::singletonInstance->scannerPin) == HIGH ? Lighting::SCANNER_LIGHT : Lighting::SCANNER_DARK;
  if (Lighting::singletonInstance->lastScannerState == scannerState) {
    return;
  }

  Lighting::singletonInstance->lastScannerState = scannerState;
  if (scannerState == Lighting::SCANNER_DARK) {
    Lighting::singletonInstance->lastScannerLightDarkChangeMicros = nowMicros;
    if (Lighting::singletonInstance->lastScannerDarkLightChangeMicros != 0) {
      uint32_t delta = (Lighting::singletonInstance->lastScannerLightDarkChangeMicros - Lighting::singletonInstance->lastScannerDarkLightChangeMicros);
      Lighting::singletonInstance->lightTimeMicros += delta;
      Lighting::singletonInstance->lightSquaredTimeMicros += delta*delta;
      Lighting::singletonInstance->lightCount++;
    }
  } else {
    Lighting::singletonInstance->lastScannerDarkLightChangeMicros = nowMicros;

    if (Lighting::singletonInstance->lastScannerLightDarkChangeMicros != 0) {
      uint32_t delta = (Lighting::singletonInstance->lastScannerDarkLightChangeMicros - Lighting::singletonInstance->lastScannerLightDarkChangeMicros);
      Lighting::singletonInstance->darkTimeMicros += delta;
      Lighting::singletonInstance->darkSquaredTimeMicros += delta*delta;
      Lighting::singletonInstance->darkCount++;
    }
  }
}*/

void Lighting::loop()
{
  if (disablePin != -1 && digitalRead(disablePin) == LOW)
  {
    driverManager->clearAllPixels();
    driverManager->show();
    return;
  }

  if (resetRequested) {
    this->reset();
  }
  
  if (loopStartMicros == 0)
  {
    loopStartMicros = system_get_time();
  }

  if (leds != NULL) {
    for (int i = 0; i < ledsCount; i++) {
      LED* led = leds[i];
    
      uint16_t intensity = this->intensity(led->driver, led->address, led->channel, repetition);
      uint16_t effectiveIntensity = led->characteristic->convertIntensity(intensity, driverManager->getDriver(led->driver)->getMaxIntensity());
//Serial.printf("LED %i: intensity %u effectiveIntensity %u\n", i, intensity, effectiveIntensity);      
      driverManager->getDriver(led->driver)->setIntensity(led->address, led->channel, effectiveIntensity);
    }
  }

  driverManager->show();
}

void Lighting::reset()
{
  Serial.println("Performing reset...");
  first = NULL;
  leds = NULL;
  ledsCount = 0;
  maxTime = -1;
  scannerEnabled = false;
  darkLightPostDelay = 0;
  repetition = -1;
  driverManager->clearAllPixels();
  resetRequested = false;
}

void Lighting::requestReset() {
  if (leds != NULL) {
    Serial.println("Requesting reset...");
    resetRequested = true;   
  }
}

void Lighting::postProcessEntries()
{
    LightingEntry* entry = this->first;
    while (entry != NULL)
    {
//      this->minAddress = entry->getAddress() < this->minAddress ? entry->getAddress() : this->minAddress;
//      this->maxAddress = entry->getAddress() > this->maxAddress ? entry->getAddress() : this->maxAddress;
      this->maxTime = this->maxTime < entry->end ? entry->end : this->maxTime;
      
      entry = entry->next;
    }

    Serial.printf("postProcessEntries finished: Using %i LEDs with a max time of %i.\n", this->ledsCount, this->maxTime);
}

void Lighting::parse(String indexEntry, String program)
{
  this->reset();
  LightingEntry* last = NULL;

  Serial.println("Start parsing ");
  Serial.println(indexEntry);
  Serial.println("---------");
  Serial.println(program);
  Serial.println("---------");

  int beginIndex = 0;
  int endIndex = 0;
  while (true)
  {
    endIndex = indexEntry.indexOf('\n', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    String line = indexEntry.substring(beginIndex, endIndex);
    line.trim();

//    Serial.print("Processing next line %s\n", line);

    // Process line.
    if (!line.startsWith("#")) {
      this->parseIndexLine(line);
    }

    beginIndex = endIndex + 1;
  }

  beginIndex = 0;
  endIndex = 0;
  while (true)
  {
    endIndex = program.indexOf('\n', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    String line = program.substring(beginIndex, endIndex);
    line.trim();

    //Serial.print("Processing next line %s\n", line);

    // Process line.
    if (!line.startsWith("#")) {
      this->parseLine(line, &last);
    }

    beginIndex = endIndex + 1;
  }

  Serial.println("Done parsing.");

  this->postProcessEntries();
  
  loopStartMicros = 0;
}

void Lighting::parseIndexLine(String line) {
  if (line.startsWith(INDEX_ENTRY_LED)) {
    String ledEntry = line.substring(INDEX_ENTRY_LED.length());
    ledEntry.trim();
    ledEntry = ledEntry+";";
    LED* led = new LED();

    int beginIndex = 0;
    int endIndex = 0;
    int partIdx = 0;
    while (true)
    {
      endIndex = ledEntry.indexOf(';', beginIndex);
      if (endIndex == -1)
      {
        break;
      }
  
      String part = ledEntry.substring(beginIndex, endIndex);
//Serial.printf("Part %s %s %i %i\n", ledEntry.c_str(), part.c_str(), beginIndex, endIndex, partIdx);
      part.trim();
      switch (partIdx) {
        case 0: 
          if (part.indexOf(':') == -1) {
            int value = part.toInt();
            led->driver = 1;
            led->address = value >> 2;
            led->channel = value & 0x3;
          } else {
            char* parts = const_cast<char*>(strtok(const_cast<char*>(part.c_str()), ":"));
            if (parts != NULL) {
              led->driver = strtol(parts, NULL, 10);
              led->address = strtol(strtok(NULL, ":"), NULL, 10);
              led->channel = strtol(strtok(NULL, ":"), NULL, 10);
      //Serial.printf("Parsed LED %i %i %i\n", led->driver, led->address, led->channel);
            } else {
              Serial.printf("Cannot parse LED descriptor '%s', ignoring.", part.c_str());
              return; 
            }
          }
          break;
        case 4: 
          led->characteristic = this->driverManager->findCharacteristic(part);         
          break;
      }

      if (led->characteristic == NULL) {
        led->characteristic = this->driverManager->findCharacteristic("Linear");         
      }
  
      beginIndex = endIndex + 1;
      partIdx++;
    }
  
    if (leds == NULL) {
      leds = new LED*[1];
      leds[0] = led;
    } else {
      LED** newLeds = new LED*[1+ledsCount];
      memcpy(newLeds, leds, ledsCount*sizeof(leds[0]));
      newLeds[ledsCount] = led;
      leds = newLeds;
    }
    ledsCount++;
    
  } else if (line.startsWith(INDEX_ENTRY_REPETITION)) {
    this->repetition = line.substring(INDEX_ENTRY_REPETITION.length()).toInt();
  } else if (line.startsWith(INDEX_ENTRY_SCANNER)) {
    String scannerEnabledStr = line.substring(INDEX_ENTRY_SCANNER.length());
    scannerEnabledStr.trim();
    this->scannerEnabled = scannerEnabledStr.equalsIgnoreCase("true");
  } else if (line.startsWith(INDEX_ENTRY_SCANNER_DARK_LIGHT_DELAY)) {
    this->darkLightPostDelay = line.substring(INDEX_ENTRY_SCANNER_DARK_LIGHT_DELAY.length()).toInt();
  }
}

void Lighting::parseLine(String arg, LightingEntry** last) {
  LightingEntry* entry = new LightingEntry();
  String line = arg + ";";
  int columnIndex = 0;
  int beginIndex = 0;
  int endIndex = 0;
  while (true)
  {
    endIndex = line.indexOf(';', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    int start;
    int end;

    String part = line.substring(beginIndex, endIndex);
    part.trim();

    switch (columnIndex) {
      case 0: // Address and channel (address << 2 + channel) 
        {
          int ledId = part.toInt();
          entry->driver = leds[ledId]->driver;
          entry->address = leds[ledId]->address;
          entry->channel = leds[ledId]->channel;
          Serial.printf("LedId is %i, resulting in driver %i, address %i and channel %i.\n", ledId, entry->driver, entry->address, entry->channel);
        }
        break;
      case 1: // Start
        Serial.print("Start is at ");
        Serial.println(part.toInt());
        entry->start = part.toInt();
        break;
      case 2: // End
        Serial.print("End is at ");
        Serial.println(part.toInt());
        entry->end = part.toInt();
        break;
      case 3: // Function
        Serial.print("Function is ");
        Serial.println(part);
        if (STR_FUNCTION_CONST.equals(part)) {
          entry->function = FUNCTION_CONST;
        } else if (STR_FUNCTION_SINF.equals(part)) {
          entry->function = FUNCTION_SINF;
        } else if (STR_FUNCTION_SINA.equals(part)) {
          entry->function = FUNCTION_SINA;
        } else if (STR_FUNCTION_STAIRCASE.equals(part)) {
          entry->function = FUNCTION_STAIRCASE;
        } else if (STR_FUNCTION_WHITENOISE.equals(part)) {
          entry->function = FUNCTION_WHITENOISE;
        }
        
        break;
      default: // Param
        int paramIndex = columnIndex - 4;
        Serial.print("Param [");
        Serial.print(paramIndex);
        Serial.print("] is ");
        Serial.println(part.toInt());
        entry->params[paramIndex] = part.toFloat();
        break;
    }

    beginIndex = endIndex + 1;
    columnIndex++;
  }

  if (first == NULL)
  {
      first = entry;
  }
  if (*last != NULL)
  {
    (*last)->next = entry;
  }
  *last = entry;
}

LightingEntry* Lighting::find(int driver, int address, int channel, int relativeTime)
{
  LightingEntry* entry = this->first;
  while (entry != NULL)
  {
    if (entry->driver == driver && entry->address == address && entry->channel == channel && relativeTime >= entry->start && relativeTime < entry->end)
    {
      return entry;
    }

    entry = entry->next;
  }

  return NULL;
}

uint16_t Lighting::intensity(int driver, int address, int channel, int repetition)
{
  uint32_t nowMicros = system_get_time();
  uint32_t millis = (nowMicros - loopStartMicros)/1000;
  
  // Figure out if we need to go black because of repetitions.
  if (repetition != -1 && millis >= repetition * this->maxTime)
  {
    // We've completed the program for as many repetitions configured, so we go dark.
    return 0;
  }

  // Figure out if we need to go black because of the scanner state.
  if (digitalRead(Lighting::singletonInstance->scannerPin) == HIGH)
  {
    // We need to go black here.
    return 0;
  }

  return this->channelIntensity(driver, address, channel, millis);
}

uint16_t Lighting::channelIntensity(int driver, int address, int channel, int absoluteTime)
{
  int time = absoluteTime % this->maxTime;

  LightingEntry* entry = this->find(driver, address, channel, time);
  if (entry == NULL)
  {
    // Paint it black :)
    return 0;
  }

  if (this->FUNCTION_CONST == entry->function)
  {
    //Serial.println((int)(entry->params[0]));
    return std::min(entry->params[0], 100)*65535/100;
  }
  else if (this->FUNCTION_SINF == entry->function)
  {
    float maxIntensity = std::min(entry->params[0], 100);
    float startF = entry->params[1];
    float endF = entry->params[2];
    float duration = entry->end - entry->start;
    float currentF = ((time - entry->start) / duration * (endF - startF)) + startF;
    return (1 + sin(2 * PI * currentF * time / 1000)) / 2 * maxIntensity * 65535/100;
  }
  else if (this->FUNCTION_SINA == entry->function)
  {
    float startA = std::min(entry->params[0], 100);
    float endA = std::min(entry->params[1], 100);
    float frequency = entry->params[2];
    float duration = entry->end - entry->start;
    float currentA = ((time - entry->start) / duration * (endA - startA)) + startA;
//    Serial.print("sina ");
//    Serial.println((1 + sin(2 * PI * frequency * time / 1000)) / 2 * currentA);
    return (1 + sin(2 * PI * frequency * time / 1000)) / 2 * currentA * 65535/100;   
  }  
  else if (this->FUNCTION_STAIRCASE == entry->function)
  {
    float startA = std::min(entry->params[0], 100);
    float endA = std::min(entry->params[1], 100);
    float steps = entry->params[2];
    float duration = entry->end - entry->start;
    int stepNumber = (floor)(((float)(time - entry->start)) * steps / duration);
    uint16_t result = (stepNumber * (endA - startA)/steps + startA) * 65536/100; 
//Serial.printf("staircase: step # %i | result %u\n", stepNumber, result); 
    return result;
  }
  else if (this->FUNCTION_WHITENOISE == entry->function)
  {
    float intensity = std::min(entry->params[0], 100);
    // Get 16 bit white noise value
    uint16_t result = (this->readRNGDataReg() >> 16)*intensity/100;
//Serial.printf("whitenoise: returning %u\n", result);
    return result; 
  }

  // Nothing found?
  return 0;
}

/*String Lighting::toMorse(char ch)
{
  switch (ch) 
  {
    case '0':
      return "- - - - -";
    case '1':
      return ". - - - -";
    case '2':
      return ". . - - -";
    case '3':
      return ". . . - -";
    case '4':
      return ". . . . -";
    case '5':
      return ". . . . .";
    case '6':
      return "- . . . .";
    case '7':
      return "- - . . .";
    case '8':
      return "- - - . .";
    case '9':
      return "- - - - .";
    case '.':
      return ". - . - . -";
  }   

  return "";
}*/

/*void Lighting::morse(Adafruit_NeoPixel* pixel, String text)
{
    this->reset();

    String code = "";
    for (int i = 0; i < text.length(); i++)
    {
        if (i > 0) 
        {
            code += "_";
        }
        code += this->toMorse(text.charAt(i));  
    }

    Serial.print("Morsing ");
    Serial.println(code);

    LightingEntry* last = NULL;
    int begin = 0;
    for (int i = 0; i < code.length(); i++)
    {
        if (code.charAt(i) == '.') 
        {
            addOn(&begin, LENGTH_DIT, &last);
        }
        else if (code.charAt(i) == '-')
        {
            addOn(&begin, LENGTH_DAT, &last);    
        }
        else if (code.charAt(i) == ' ')
        {
            addOff(&begin, LENGTH_DIT, &last);    
        }
        else if (code.charAt(i) == '_')
        {
            addOff(&begin, LENGTH_DAT, &last);    
        }

//        Serial.print("Begin now at ");
//        Serial.println(begin);
    }

    addOff(&begin, 7*LENGTH_DIT, &last);

    Serial.println("Done morsings.");

    this->postProcessEntries();
    
    loopStartMicros = 0;
}*/

/*void Lighting::addOn(int* begin, int length, LightingEntry** last)
{
    LightingEntry* entry = new LightingEntry();
    entry->driver = 0;
    entry->address = 0;
    entry->channel = 0;
    entry->function = FUNCTION_CONST;
    entry->params[0] = 255;
    entry->start = *begin;
    entry->end = *begin + length;    
    *begin = entry->end;       
    if (first == NULL)
    {
        first = entry;
    }
    if (*last != NULL)
    {
        (*last)->next = entry;
    }
    *last = entry;
}

void Lighting::addOff(int* begin, int length, LightingEntry** last)
{
    LightingEntry* entry = new LightingEntry();
    entry->driver = 0;
    entry->address = 0;
    entry->channel = 0;
    entry->function = FUNCTION_CONST;
    entry->params[0] = 0;
    entry->start = *begin;
    entry->end = *begin + length;           
    *begin = entry->end;
    if (first == NULL)
    {
        first = entry;
    }
    if (*last != NULL)
    {
        (*last)->next = entry;
    }
Serial.printf("*last %lu\n", *last);
    *last = entry;
}*/

void Lighting::addScannerData(JsonObject& root) {
  root["darkCount"] = darkCount;
  root["lightCount"] = lightCount;
  root["darkTimeMicros"] = darkTimeMicros;
  root["lightTimeMicros"] = lightTimeMicros;
  root["darkSquaredTimeMicros"] = darkSquaredTimeMicros;
  root["lightSquaredTimeMicros"] = lightSquaredTimeMicros;
}

void Lighting::clearScannerData() {
  darkCount = 0;
  lightCount = 0;
  darkTimeMicros = 0;
  lightTimeMicros = 0;
  darkSquaredTimeMicros = 0;
  lightSquaredTimeMicros = 0;
}
