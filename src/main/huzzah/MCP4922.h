#ifndef MCP4922_h
#define MCP4922_h

#include "DriverManager.h"

class MCP4922 : public Driver {
  private:
    const uint32_t CHANNEL_MASKS[2] = {0b0011000000000000, 0b1011000000000000};
    int csPin;
    int channelValues[2] = {0, 0};
  public:
    MCP4922(String name, int csPin);
    String getType();
    int getAddressCount();
    int getChannelCount();
    uint16_t getMaxIntensity();
    uint16_t getIntensity(int address, int channel) ;
    void setIntensity(int address, int channel, uint16_t intensity);
    void show();
    void toggle(int address, int channel);
};

#endif
