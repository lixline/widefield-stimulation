#include "Configuration.h"
#include "ISC.h"
#include "Lighting.h"
#include "Programs.h"
#include <SPIFFS.h>
#include "WebServ.h"
#include <WiFi.h>

#ifdef ESP8266
extern "C" {
#include "user_interface.h"
}
#endif

ISC* isc;
DriverManager* driverManager;
Configuration* conf;
Lighting* lighting;
Programs* programs;
Webserver* webserver;

IPAddress local_IP(192, 168, 4, 1);
IPAddress gateway(192, 168, 4, 1);
IPAddress subnet(255, 255, 255, 0);

int wifiClientCount = -1;

int lastAnalogIn = -999;

int loopCount = 0;
uint32_t loopStart = system_get_time();

void setup() {
  //system_update_cpu_freq(160);
  Serial.begin(112500);
  //Serial.printf("CPU Frequency %i\n", system_get_cpu_freq());

  SPIFFS.begin();

  isc = new ISC();
  driverManager = new DriverManager;
  conf = new Configuration(driverManager);

  if (!conf->getWifiSSID().equals("") && !conf->getWifiPassword().equals("") )
  {
    if (conf->isWifiStation())
    {
      Serial.printf("Trying to connect to SSID '%s'.\n", conf->getWifiSSID().c_str());
      WiFi.mode(WIFI_STA);
      WiFi.begin(conf->getWifiSSID().c_str(), conf->getWifiPassword().c_str());
      if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.println("Connection Failed! Rebooting...");
        delay(5000);
        ESP.restart();
      }
    }
    else
    {
      if (conf->getWifiPowerAdjustable())
      {
        Serial.printf("Setting output power to %f\n", conf->getWifiPower());
//        WiFi.setOutputPower(conf->getWifiPower());
      }
      Serial.printf("Starting AP for SSID '%s'.\n", conf->getWifiSSID().c_str());
      WiFi.mode(WIFI_AP);
      WiFi.softAPConfig(local_IP, gateway, subnet);
      //      WiFi.setAutoConnect(false);
      WiFi.softAP(conf->getWifiSSID().c_str(), conf->getWifiPassword().c_str(), 7);
    }
  }

  // put your setup code here, to run once:
  if (conf->getDisablePin() != -1) {
    pinMode(conf->getDisablePin(), INPUT);
  }
  if (conf->getScannerPin() != -1) {
    pinMode(conf->getScannerPin(), INPUT);
  }
  if (conf->getLedsPin() != -1) {
    pinMode(conf->getLedsPin(), OUTPUT);
  }
  //pinMode(conf->getIpPin(), OUTPUT);


  lighting = new Lighting(driverManager, conf->getDisablePin(), conf->getScannerPin(), isc);
  programs = new Programs(lighting);
  webserver = new Webserver(conf, driverManager, programs, lighting);

  webserver->setup();

  String ip = conf->isWifiStation() ? WiFi.localIP().toString() : WiFi.softAPIP().toString();
  Serial.printf("Listening at IP %s.\n", ip.c_str());
  Serial.printf("Free heap %i bytes.\n", ESP.getFreeHeap());
  Serial.printf("Lighting entry size is %i bytes.\n", sizeof(LightingEntry));

//  MDNS.begin("Widefield");
//  MDNS.addService("arduino", "tcp", 8266);
  // send new parameter to LED

  //lighting->morse(NULL, ip);

  //programs->activate(0);


  //lighting->setPixelColorAndShow(0, 0, 255, 0);

}

void loop()
{
  isc->loop();
  lighting->loop();

  /*if (!conf->isWifiStation())
    {
    int stationCount = WiFi.softAPgetStationNum();
    if (wifiClientCount != stationCount)
    {
      Serial.printf("Station count is now %i.\n", stationCount);
    }
    }*/

  loopCount += 1;
  //Serial.printf("LoopCount %i %i\n", loopCount, loopCount & 0x0fff);
   uint32_t duration = system_get_time() - loopStart;
  if (duration > 10000000) {
    Serial.printf("Frequency [kHz]: %f\n", 1000 * ((float)loopCount / (float)duration));

    loopStart = system_get_time();
    loopCount = 0;
  }
}
