#ifndef DriverManager_h
#define DriverManager_h

#include <Arduino.h>
#include <ArduinoJson.h>
#include <map>

class Driver {
  private:
    String name;
  protected:
    Driver(String name);
  public:
    String getName();
    virtual String getType() = 0;
    virtual int getAddressCount() = 0;
    virtual int getChannelCount() = 0;
    virtual uint16_t getMaxIntensity() = 0;
    virtual uint16_t getIntensity(int address, int channel) = 0;
    virtual void setIntensity(int address, int channel, uint16_t intensity) = 0;
    virtual void show() = 0;
    virtual void toggle(int address, int channel) = 0;
};

class Characteristic {
  private:
    String name;
  protected:
    Characteristic(String name);
  public: 
    String getName();
    virtual uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity) = 0;
};

class DriverManager {
  private: 
    int driverCount = 0;
    Driver** drivers;
    int characteristicCount = 0;
    Characteristic** characteristics;
  public:
    DriverManager();
    void registerDriverConfig(String driverConfigName, String driverConfig);
    void addDriverConfig(JsonObject& root);
    void addActiveLEDs(JsonObject& root);
    void clearAllPixels();
    void show();  
    Driver* getDriver(int i);
    Characteristic* findCharacteristic(String name);
};

class Linear : public Characteristic {
  public:
    Linear();
    uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity);
};

class LED590_33W_AB_593nm : public Characteristic {
  public:
    LED590_33W_AB_593nm();
    uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity);
};

class RLS_5B_475_S_AB_484nm : public Characteristic {
  public:
    RLS_5B_475_S_AB_484nm();
    uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity);
};

class VL415_5_15_A_68_Ohm_420nm : public Characteristic {
  public:
    VL415_5_15_A_68_Ohm_420nm();
    uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity);
};

class VL415_5_15_B_22_Ohm_420nm : public Characteristic {
  public:
    VL415_5_15_B_22_Ohm_420nm();
    uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity);
};

class RLT_360_15_30_3CC_15_Ohm_363nm : public Characteristic {
  public:
    RLT_360_15_30_3CC_15_Ohm_363nm();
    uint16_t convertIntensity(uint16_t intensity, uint16_t maxIntensity);
};

#endif
