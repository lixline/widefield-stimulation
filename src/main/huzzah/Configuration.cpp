#include <Arduino.h>
#include "Configuration.h"
#include <SPIFFS.h>

Configuration::Configuration(DriverManager* driverManager)
{
  this->driverManager = driverManager;
  
  File f = SPIFFS.open("/config.properties", "r");
  if (!f) {
    Serial.println("Cannot load config.properties, cannot connect to Wifi.");
  }

  String config = "";
  while (f.available()){
      config += char(f.read());
  }
  f.close();

  int beginIndex = 0;
  int endIndex = 0;
  while (true)
  {
    endIndex = config.indexOf('\n', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    String line = config.substring(beginIndex, endIndex);
    line.trim();
    
    // Process line.
    if (line.startsWith(CONFIG_WIFI_MODE)) {
      String modeStr = line.substring(CONFIG_WIFI_SSID.length());
      modeStr.trim();
      if (modeStr == String("station")) {
        wifiStation = true;
      } else if (modeStr == String("ap")) {
        wifiStation = false;
      } else {
        Serial.printf("Cannot parse wifi mode from '%s', falling back to 'station'.", modeStr.c_str());
        wifiStation = true;
      }
      Serial.printf("WiFi mode '%s'\n", String(wifiStation ? "station" : "ap").c_str());
    } else if (line.startsWith(CONFIG_WIFI_SSID)) {
      ssid = line.substring(CONFIG_WIFI_SSID.length());
      ssid.trim();
      Serial.printf("SSID '%s'\n", ssid.c_str());
    } else if (line.startsWith(CONFIG_WIFI_KEY)) {
      password = line.substring(CONFIG_WIFI_KEY.length());
      password.trim();
      Serial.printf("Password (defined)\n");
    } else if (line.startsWith(CONFIG_WIFI_ADJUSTABLE_POWER)) {
      String wifiPowerAdjustableStr = line.substring(CONFIG_WIFI_ADJUSTABLE_POWER.length());
      wifiPowerAdjustable = wifiPowerAdjustableStr.equalsIgnoreCase("true");
      Serial.printf("Wifi power adjustable%s\n", wifiPowerAdjustable ? "true" : "false");      
    } else if (line.startsWith(CONFIG_LEDS_COUNT)) {
      ledsCount = line.substring(CONFIG_LEDS_COUNT.length()).toInt();
      Serial.printf("LEDs count %i\n", ledsCount);
    } else if (line.startsWith(CONFIG_LEDS_PIN)) {
      ledsPin = line.substring(CONFIG_LEDS_PIN.length()).toInt();
      Serial.printf("LEDs pin %i\n", ledsPin);
    } else if (line.startsWith(CONFIG_DISABLE_PIN)) {
      ipPin = line.substring(CONFIG_DISABLE_PIN.length()).toInt();
      Serial.printf("Disable pin %i\n", disablePin);
    } else if (line.startsWith(CONFIG_IP_PIN)) {
      ipPin = line.substring(CONFIG_IP_PIN.length()).toInt();
      Serial.printf("IP pin %i\n", ipPin);
    } else if (line.startsWith(CONFIG_SCANNER_PIN)) {
      scannerPin = line.substring(CONFIG_SCANNER_PIN.length()).toInt();
      Serial.print("Scanner pin ");
      Serial.println(scannerPin);
    } else if (line.startsWith(CONFIG_APPLICATION_NAME)) {
      applicationName = line.substring(CONFIG_APPLICATION_NAME.length());
      applicationName.trim();
      Serial.print("Application name '");
      Serial.print(applicationName);
      Serial.println("'");
    } else if (line.startsWith(CONFIG_APPLICATION_LOGO)) {
      applicationLogo = line.substring(CONFIG_APPLICATION_LOGO.length());
      applicationLogo.trim();
      Serial.print("Application logo '");
      Serial.print(applicationLogo);
      Serial.println("'");
    } else if (line.startsWith(CONFIG_DRIVER)) {
      String driverLine = line.substring(CONFIG_DRIVER.length());
      driverLine.trim();
      int index = driverLine.indexOf("=");
      if (index != -1) {
        String driverConfigName = driverLine.substring(0, index);
        driverConfigName.trim();
        String driverConfig = driverLine.substring(index+1);
        driverConfig.trim();
        driverManager->registerDriverConfig(driverConfigName, driverConfig);
      }
    
    } else if (!line.startsWith("#") && line.length() > 0) {
      Serial.printf("Ignoring config line '%s'.\n", line.c_str());
    }

    beginIndex = endIndex + 1;
  }

}

boolean Configuration::isWifiStation()
{
  return this->wifiStation;
}

String Configuration::getWifiSSID()
{
  return this->ssid;
}

String Configuration::getWifiPassword()
{
  return this->password;
}

int Configuration::getLedsCount()
{
  return this->ledsCount;
}

int Configuration::getLedsPin()
{
  return this->ledsPin;
}

int Configuration::getDisablePin()
{
  return this->disablePin;
}

/*int Configuration::getIpPin()
{
  return this->ipPin;
}*/

int Configuration::getScannerPin()
{
  return this->scannerPin;
}

float Configuration::getWifiPower()
{
  if (wifiPower == -1) {
      int analogIn = analogRead(A0); 
      // 1000 -> 82 (20,5dbm), 0 -> 0dbm
      wifiPower = ((float)analogIn)/48;
  }
  
  return wifiPower;
}

boolean Configuration::getWifiPowerAdjustable()
{
  return this->wifiPowerAdjustable;
}
