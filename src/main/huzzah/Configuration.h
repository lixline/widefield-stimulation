#ifndef Configuration_h
#define Configuration_h

#include <Arduino.h>
#include "DriverManager.h"

class Configuration {
  private:
    String CONFIG_WIFI_MODE = "wifi.mode=";
    String CONFIG_WIFI_SSID = "wifi.ssid=";
    String CONFIG_WIFI_KEY = "wifi.key=";
    String CONFIG_WIFI_ADJUSTABLE_POWER = "wifi.adjustable_power=";
    String CONFIG_LEDS_COUNT = "leds.count=";
    String CONFIG_LEDS_PIN = "leds.pin=";
    String CONFIG_SCANNER_PIN = "scanner.pin=";
    String CONFIG_DISABLE_PIN = "disable.pin=";
    String CONFIG_IP_PIN = "ip.pin=";
    String CONFIG_APPLICATION_NAME = "application.name=";
    String CONFIG_APPLICATION_LOGO = "application.logo=";
    String CONFIG_DRIVER = "driver.";

    DriverManager* driverManager;
    boolean wifiStation = true;
    boolean wifiPowerAdjustable = false;
    String ssid = "";
    String password = "";
    int ledsCount = -1;
    int ledsPin = -1;
    int disablePin = -1;
    int ipPin = -1;
    int scannerPin = -1;
    String applicationName = "";
    String applicationLogo = "";
    float wifiPower = -1;

  public:
    Configuration(DriverManager* driverManager);
    boolean isWifiStation();
    String getWifiSSID();
    String getWifiPassword();
    boolean getWifiPowerAdjustable();
    int getLedsCount();
    int getLedsPin();
    int getDisablePin();
    //int getIpPin();
    float getWifiPower();
    int getScannerPin();
};
#endif

