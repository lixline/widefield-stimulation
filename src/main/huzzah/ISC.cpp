#include <Arduino.h>
#include "ISC.h"

inline uint32_t ISC::getCycleCount() {
  uint32_t ccount;
  __asm__ __volatile__("rsr %0,ccount":"=a" (ccount));
  return (uint32_t)ccount;
}



void ISC::measureCyclesPerMicro() {
  if (cyclesPerMicro != NULL)
  {
    return;
  }
  
  static uint32_t cc0 = NULL;
  static uint32_t micros0 = NULL;
  static uint32_t cc = NULL;
  static uint32_t micros = NULL;
  if (cc0 == NULL) {
    cc0 = getCycleCount();
    micros0 = system_get_time();
  } else {
    uint32_t newCC = getCycleCount();
    if (newCC < cc0) {
      cyclesPerMicro = ((float)(cc - cc0))/(micros - micros0);
    } else {
      micros = system_get_time(); 
      cc = newCC;
    }
  }
}

void ISC::adjustLastRollover() {
  if (cyclesPerMicro == NULL)
  {
    return;
  }

  static uint32_t lastCC = NULL;
  uint32_t cc = getCycleCount();
  if (cc < lastCC) {
    lastRolloverMicros = system_get_time() - cc / cyclesPerMicro;
  } else {
    lastCC = cc;
  }
}

void ISC::loop() {
  if (cyclesPerMicro == NULL)
  {
    measureCyclesPerMicro();
  }
  else
  {
    adjustLastRollover();
  }
}

uint32_t IRAM_ATTR ISC::getInterruptSafeTime() {
   return lastRolloverMicros + getCycleCount() / cyclesPerMicro;
}

boolean IRAM_ATTR ISC::isReady() {
  return cyclesPerMicro != NULL && lastRolloverMicros != NULL;
}

