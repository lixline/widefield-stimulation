#include <Arduino.h>
#include "Neopixel.h"

Neopixel::Neopixel(String name, int pin, int count) : Driver(name)
{
  Serial.printf("Neopixel driver at pin %i with %i LEDs created.\n", pin, count);  
  pinMode(pin, OUTPUT);
  this->pixel = new Adafruit_NeoPixel(count, pin, NEO_GRB + NEO_KHZ800);
}

String Neopixel::getType() {
  return "Neopixel";
}

int Neopixel::getAddressCount() {
  return pixel->numPixels();
}
    
int Neopixel::getChannelCount() {
  return 3;
}

uint16_t Neopixel::getMaxIntensity() {
  return 255;
}

void Neopixel::toggle(int address, int channel) {
//Serial.printf("toggle %i/%i\n", address, channel);  
  uint32_t andMask = 0xff << (channel*8);
//Serial.printf("toggle: Color was %08lx.\n", pixel->getPixelColor(address));
  int newValue = (pixel->getPixelColor(address) & andMask) == 0 ? this->getMaxIntensity() : 0;
  setIntensity(address, channel, newValue);
}

void Neopixel::show() {
  if (modified) {
    pixel->show();
    modified = false;
  }
}

uint16_t Neopixel::getIntensity(int address, int channel) {
  uint32_t color = pixel->getPixelColor(address);
  uint32_t andMask = ~(0xff << (channel*8));
  uint32_t intensity = color && andMask >> (channel*8);
  return intensity;
}

void Neopixel::setIntensity(int address, int channel, uint16_t intensity) {
//Serial.printf("setIntensity %i/%i -> %i\n", address, channel, intensity);  
  uint32_t prevColor = pixel->getPixelColor(address);
  uint32_t andMask = ~(0xff << (channel*8));
  uint32_t orMask = intensity << (channel*8);
  uint32_t newColor = (prevColor & andMask) | orMask;
//Serial.printf("Color was %08lx, andMask %08lx, orMask %08lx resulting in new color %08lx.\n", prevColor, andMask, orMask, newColor);
  if (newColor != prevColor) {
    modified = true;
    pixel->setPixelColor(address, newColor);
  }
}

