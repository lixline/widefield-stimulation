#ifndef Neopixel_h
#define Neopixel_h

#include <Adafruit_NeoPixel.h>
#include "DriverManager.h"

class Neopixel : public Driver {
  private:
    Adafruit_NeoPixel* pixel;
    boolean modified = false;
  public:
    Neopixel(String name, int pin, int count);
    String getType();
    int getAddressCount();
    int getChannelCount();
    uint16_t getMaxIntensity();
    uint16_t getIntensity(int address, int channel);
    void setIntensity(int address, int channel, uint16_t intensity);
    void show();
    void toggle(int address, int channel);
};

#endif
