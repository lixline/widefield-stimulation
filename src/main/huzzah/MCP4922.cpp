#include <Arduino.h>
#include "MCP4922.h"
#include <SPI.h>

MCP4922::MCP4922(String name, int csPin) : Driver(name)
{
  this->csPin = csPin;
  Serial.printf("MCP4922 driver with CS %i created.\n", csPin);  
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setFrequency(20000000);
  pinMode(csPin, OUTPUT);
}

String MCP4922::getType() {
  return "MCP4922";
}

int MCP4922::getAddressCount() {
  return 1;
}
    
int MCP4922::getChannelCount() {
  return 2;
}

uint16_t MCP4922::getMaxIntensity() {
  return 4095;
}

void MCP4922::show() {
  
}

void MCP4922::setIntensity(int address, int channel, uint16_t intensity) {
  // No change.
  if (intensity == channelValues[channel]) {
    return;
  }
  uint16_t writeCommand = std::min(intensity, this->getMaxIntensity()) | CHANNEL_MASKS[channel];
Serial.printf("Writing channel %i %u %u...\n", channel, intensity, writeCommand);
  digitalWrite(csPin, LOW);
  SPI.transfer(highByte(writeCommand));
  SPI.transfer(lowByte(writeCommand));
  digitalWrite(csPin, HIGH);
//Serial.printf("done.\n");

  channelValues[channel] = intensity;
}

uint16_t MCP4922::getIntensity(int address, int channel) {
  return channelValues[channel];
}

void MCP4922::toggle(int address, int channel) {
//  Serial.println("Toggle invoked.");
  int newValue = channelValues[channel] == 0 ? this->getMaxIntensity() : 0;
  setIntensity(address, channel, newValue);
}
