#include <Arduino.h>
#include "Lighting.h"
#include "Programs.h"
#include <SPIFFS.h>

Programs::Programs(Lighting* lighting)
{
  this->lighting = lighting;
}

String Programs::readFile(String fileName)
{
  File f = SPIFFS.open(fileName, "r");
  if (!f) {
    Serial.printf("readFile: Cannot open file '%s', returning ''.\n", fileName.c_str());
    return "";
  }

  String content = "";
  while (f.available()) {
    content += char(f.read());
  }
  f.close();
  Serial.printf("readFile: Read file '%s', returning %i bytes.\n", fileName.c_str(), content.length());
  return content;
}

bool Programs::writeFile(String fileName, String content)
{
  File f = SPIFFS.open(fileName, "w");
  if (!f) {
    return false;
  }

  f.println(content);
  f.close();
}

String Programs::getIndexEntry(int id)
{
  String index = this->getIndex();
  String result = "";

  int beginIndex = 0;
  int endIndex = 0;
  bool insideCorrectSection = false;
  while (true)
  {
    endIndex = index.indexOf('\n', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    String line = index.substring(beginIndex, endIndex);
    line.trim();

    // Process line.
    if (line.startsWith("[") && line.endsWith("]")) {
      int section = line.substring(1, line.length() - 1).toInt();
      insideCorrectSection = section == id;
    }
    else if (insideCorrectSection)
    {
      result += line;
      result += "\n";
    }

    beginIndex = endIndex + 1;
  }
  return result;
}

int Programs::getNextProgramId()
{
  String index = this->getIndex();

  int beginIndex = 0;
  int endIndex = 0;
  int maxIndex = -1;
  while (true)
  {
    endIndex = index.indexOf('\n', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    String line = index.substring(beginIndex, endIndex);
    line.trim();

    if (line.startsWith("[") && line.endsWith("]")) {
      int section = line.substring(1, line.length() - 1).toInt();
      maxIndex = (section > maxIndex) ? section : maxIndex;
    }

    beginIndex = endIndex + 1;
  }

  // Program 0 should never be overwritten!
  return maxIndex == -1 ? 1 : maxIndex + 1;
}


String Programs::getUpdatedIndex(int id, String newIndexEntry)
{
  String index = this->getIndex();
  String result = "";

  int beginIndex = 0;
  int endIndex = 0;
  bool insideCorrectSection = false;
  bool found = false;
  while (true)
  {
    endIndex = index.indexOf('\n', beginIndex);
    if (endIndex == -1)
    {
      break;
    }

    String line = index.substring(beginIndex, endIndex);
    line.trim();

    // Process line.
    if (line.startsWith("[") && line.endsWith("]")) {
      result += line;
      result += "\n";
      int section = line.substring(1, line.length() - 1).toInt();
      Serial.print("Found section ");
      Serial.print(section);
      Serial.print(" from ");
      Serial.println(line);
      if (section == id) {
        insideCorrectSection = true;
        found = true;
        result += newIndexEntry;
        if (!result.endsWith("\n")) {
          result += "\n";
        }
      } else {
        insideCorrectSection = false;
      }
    }
    else if (!insideCorrectSection)
    {
      result += line;
      result += "\n";
    }

    beginIndex = endIndex + 1;
  }

  if (!found)
  {
    result += "[";
    result += id;
    result += "]";
    result += "\n";
    result += newIndexEntry;
    result += "\n";
  }

  return result;
}

String Programs::getIndex()
{
  return this->readFile("/index");
}

String Programs::getConfigProperties()
{
  return this->readFile("/config.properties");
}

String Programs::read(int id) {
  String fileName = "/";
  fileName.concat(id);
  fileName.concat(".lig");
  Serial.print("Reading file ");
  Serial.println(fileName);

  String result = this->readFile(fileName);
  result.concat("-------\n");
  result.concat(this->getIndexEntry(id));
  return result;
}

bool Programs::create(String content)
{
  int id = getNextProgramId();
  return this->write(id, content);
}

bool Programs::write(int id, String content)
{
  int splitAt = content.indexOf("-------\n");
  if (splitAt < 0)
  {
    return false;
  }

  String program = content.substring(0, splitAt);
  String metaData = content.substring(splitAt + String("-------\n").length());

  Serial.print("Writing at ");
  Serial.print(id);
  Serial.println(":");
  Serial.println("Program:");
  Serial.println(program);
  Serial.println("Metadata:");
  Serial.println(metaData);

  String fileName = "/";
  fileName.concat(id);
  fileName.concat(".lig");
  File f = SPIFFS.open(fileName, "w");
  if (!f) {
    return false;
  }

  f.println(program);
  f.close();

  String updatedIndex = getUpdatedIndex(id, metaData);
  if (!writeFile("/index", updatedIndex))
  {
    return false;
  }

  // Did we change the active program?
  if (id == active)
  {
    lighting->parse(this->getIndexEntry(id), content);
  }
  return true;
}

bool Programs::activate(int id) {
  String fileName = "/";
  fileName.concat(id);
  fileName.concat(".lig");
  String indexEntry = this->getIndexEntry(id);
  String program = this->readFile(fileName);
  if (program.length() > 0) {
    lighting->parse(indexEntry, program);
    active = id;
    return true;
  } else {
    return false;
  }
}

void Programs::deactivate()
{
  active = -1;
  lighting->requestReset();
}

int Programs::getActive()
{
  return active;
}
