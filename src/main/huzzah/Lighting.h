#ifndef Lighting_h
#define Lighting_h

#include <Arduino.h>
#include "DriverManager.h"
#include "ISC.h"

class LightingEntry {
  public:
    int start; // ms
    int end; // ms
    int driver;
    int address;
    int channel;
    int function;
    int params[5] = {0,0,0,0,0};
    LightingEntry* next = NULL;
};

class LED {
  public:
    Characteristic* characteristic;
    int driver;
    int address;
    int channel;
};

class Lighting {
  private:
    const int LENGTH_DIT = 160;
    const int LENGTH_DAT = 3*LENGTH_DIT;
    const int LENGTH_PAUSE = 2*LENGTH_DIT;

    const String STR_FUNCTION_CONST = "const";
    const String STR_FUNCTION_SINF = "sinf";
    const String STR_FUNCTION_SINA = "sina";
    const String STR_FUNCTION_STAIRCASE = "staircase";
    const String STR_FUNCTION_WHITENOISE = "whitenoise";
    
    const int FUNCTION_CONST = 0;
    const int FUNCTION_SINF = 1;
    const int FUNCTION_SINA = 2;
    const int FUNCTION_STAIRCASE = 3;
    const int FUNCTION_WHITENOISE = 4;
    
    const String INDEX_ENTRY_NAME = "NAME: ";
    const String INDEX_ENTRY_DESCRIPTION = "DESCRIPTION: ";
    const String INDEX_ENTRY_LED = "LED: ";
    const String INDEX_ENTRY_REPETITION = "REPETITION: ";
    const String INDEX_ENTRY_SCANNER = "SCANNER: ";
    const String INDEX_ENTRY_SCANNER_DARK_LIGHT_DELAY = "SCANNER_DARK_LIGHT_POST_DELAY: ";

    DriverManager* driverManager;
    LED** leds = NULL;
    int ledsCount;
    uint32_t loopStartMicros = 0;
    LightingEntry* first = NULL;
    int ledPin;
    int disablePin;
    volatile int scannerPin;
    volatile boolean resetRequested = false;
    
    int repetition = -1;
    bool scannerEnabled = false;
    int darkLightPostDelay = 0;
    
    static void handleScannerInterrupt();
    void parseLine(String line, LightingEntry** last);      
    void parseIndexLine(String line);      
    void postProcessEntries();
    LightingEntry* find(int driver, int address, int channel, int time);
    //Adafruit_NeoPixel* pixel;
    //String toMorse(char ch);
    //void addOn(int* begin, int length, LightingEntry** last);
    //void addOff(int* begin, int length, LightingEntry** last);
    
  public:
    static const int DRAM_ATTR SCANNER_LIGHT = 0;
    static const int DRAM_ATTR SCANNER_DARK = 1;
    
    ISC* isc;
    volatile uint32_t lastScannerDarkLightChangeMicros = 0;
    volatile uint32_t lastScannerLightDarkChangeMicros = 0;
    volatile int lastScannerState = -1;
    volatile uint32_t darkTimeMicros = 0;
    volatile uint32_t lightTimeMicros = 0;
    volatile uint32_t darkSquaredTimeMicros = 0;
    volatile uint32_t lightSquaredTimeMicros = 0;
    volatile uint32_t darkCount = 0;
    volatile uint32_t lightCount = 0;
  
    static volatile Lighting* singletonInstance;
    Lighting(DriverManager* driverManager, int disablePin, int scannerPin, ISC* isc);
    //int minAddress = 100000;
    //int maxAddress = -1;
    int maxTime = -1;
    inline uint32_t readRNGDataReg();
    void reset();
    void requestReset();
    void parse(String indexEntry, String program);
    uint16_t intensity(int driver, int address, int channel, int repetition);
    uint16_t channelIntensity(int driver, int address, int channel, int time);
    //void morse(Adafruit_NeoPixel* pixel, String text);
    void loop();
    void setup();
    void addScannerData(JsonObject& root);
    void clearScannerData();
};

#endif
