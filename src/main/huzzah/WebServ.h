


#ifndef Webserver_h
#define Webserver_h

//#include <Adafruit_NeoPixel.h>
//#include <AsyncEventSource.h>
//#include <AsyncJson.h>
//#include <SPIFFSEditor.h>
//#include <WebHandlerImpl.h>
#include <ESPAsyncWebServer.h>
//#include <WebAuthentication.h>
//#include <AsyncWebSocket.h>
//#include <WebResponseImpl.h>
//#include <StringArray.h>
//#include <HTTP_Method.h>
#include "Programs.h"
#include "Configuration.h"

class Webserver {
  private:
    Configuration* configuration;
    DriverManager* driverManager;
    Programs* programs;
    Lighting* lighting;
    AsyncWebServer* server = new AsyncWebServer(80);
    void handleRoot(AsyncWebServerRequest* request);
    void handleReadConfiguration(AsyncWebServerRequest* request);
    void handleReadIndex(AsyncWebServerRequest* request);
    void handleReadConfigProperties(AsyncWebServerRequest* request);
    void handleReadProgram(AsyncWebServerRequest* request);
    void handleCreateProgram(AsyncWebServerRequest* request);
    void handleWriteProgram(AsyncWebServerRequest* request);
    void handleClearAll(AsyncWebServerRequest* request); 
    void handleActivateProgram(AsyncWebServerRequest* request);
    void handleToggle(AsyncWebServerRequest* request);
    void handleGetScanner(AsyncWebServerRequest* request);
    void handleClearScanner(AsyncWebServerRequest* request);

  public:
    Webserver(Configuration* configuration, DriverManager* driverManager, Programs* programs, Lighting* lighting);
    void setup();
};


#endif
