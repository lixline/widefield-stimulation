import React from 'react';
import ReactFauxDOM from 'react-faux-dom'
import { SketchPicker } from 'react-color';
import { Link } from 'react-router-dom';
import './Editor.css';
import { Button, Modal } from 'react-bootstrap';
import { throws } from 'assert';

class Editor extends React.Component {
    constructor(props) {
        super(props);
        var self = this;

        this.functions = {
            const: {
                arguments: [{
                    name: 'intensity', min: 0, max: 100, unit: '%'
                }]
            },
            sinf: {
                arguments: [{
                    name: 'maxIntensity', min: 0, max: 100, unit: '%'
                }, {
                    name: 'startFrequency', min: 1, max: 10000, unit: 'Hz'
                }, {
                    name: 'endFrequency', min: 1, max: 10000, unit: 'Hz'
                }]
            },
            sina: {
                arguments: [{
                    name: 'startIntensity', min: 0, max: 100, unit: '%'
                }, {
                    name: 'endIntensity', min: 0, max: 100, unit: '%'
                }, {
                    name: 'frequency', min: 1, max: 10000, unit: 'Hz'
                }]
            },
            staircase: {
                arguments: [{
                    name: 'startIntensity', min: 0, max: 100, unit: '%'
                }, {
                    name: 'endIntensity', min: 0, max: 100, unit: '%'
                }, {
                    name: 'steps', min: 1, max: 10000, unit: '1'
                }]
            }, 
            whitenoise: {
                arguments: [{
                    name: 'intensity', min: 0, max: 100, unit: '%'
                }]
            }
        }

        self.fetchConfiguration();

        if (props.match.params.programId) {
            fetch('/program?id=' + props.match.params.programId)
                .then((response) => response.text())
                .then((responseText) => {
                    var parts = responseText.split("-------\n");
                    self.setState({ program: parts[0] });
                    self.setState(this.parseMetadata(parts[1]));
                    self.setState(this.parseProgram(parts[0], self.state.leds));
                    self.adjustAddressAndChannel();
                })
                .catch((error) => {
                    console.error(error);
                });
            this.state = {
                editedProgram: props.program,
                name: "",
                description: "",
                leds: [],
                channelNames: ['0', '1', '2'],
                ledStates: [],
                modified: false
            };
        } else {
            // New program
            this.state = {
                editedProgram: "",
                name: "",
                description: "",
                leds: [],
                channelNames: ['0', '1', '2'],
                ledStates: [],
                modified: true
            }
        }

        this.parseProgram = this.parseProgram.bind(this);
        this.setLEDName = this.setLEDName.bind(this);
        this.getPersistableProgram = this.getPersistableProgram.bind(this);
        this.save = this.save.bind(this);
        this.back = this.back.bind(this);
        this.goToList = this.goToList.bind(this);
    }

    fetchConfiguration() {
        var self = this;
        return fetch('/configuration')
            .then((response) => response.json())
            .then((responseObj) => {
                self.setState({
                    ledStates: responseObj.leds || [], 
                    active: responseObj.active,
                    characteristics: responseObj.characteristics,
                    drivers: responseObj.drivers
                });
                this.adjustAddressAndChannel();
            })
            .catch((error) => {
                console.error(error);
            });
    }

    parseMetadata(metadata) {
        var result = {
            "leds": [],
            "repetition": -1,
            "scanner": false,
            "scannerDarkLightPostDelay": 100
        };
        metadata.split("\n").forEach(function (line) {
            line = line.trim();
            if (line.startsWith("NAME: ")) {
                result.name = line.substring("NAME: ".length);
            } else if (line.startsWith("DESCRIPTION: ")) {
                result.description = line.substring("DESCRIPTION: ".length);
            } else if (line.startsWith("LED: ")) {
                var led = {};
                var ledParts = line.substring("LED: ".length).split(";");
                var driverAddressAndChannel = ledParts[0].trim();
                if (driverAddressAndChannel.indexOf(":") == -1) {
                    var tmp = parseInt(driverAddressAndChannel);
                    led.driver = 0;
                    led.address = tmp >> 2;
                    led.channel = tmp & 3;
                } else {
                    var parts = driverAddressAndChannel.split(":");
                    led.driver = parseInt(parts[0]);
                    led.address = parseInt(parts[1]);
                    led.channel = parseInt(parts[2]);
                }
                led.color = ledParts[1].trim();
                led.name = ledParts[2].trim();
                led.description = ledParts[3].trim();
                led.characteristic = ledParts[4] == undefined ? "Linear" : ledParts[4].trim();
                result.leds.push(led);
            } else if (line.startsWith("REPETITION: ")) {
                result.repetition = parseInt(line.substring("REPETITION: ".length));
            } else if (line.startsWith("SCANNER: ")) {
                result.scanner = line.substring("SCANNER: ".length).trim().toLowerCase() == "true";
            } else if (line.startsWith("SCANNER_DARK_LIGHT_POST_DELAY: ")) {
                result.scannerDarkLightPostDelay = parseInt(line.substring("SCANNER_DARK_LIGHT_POST_DELAY: ".length));
            }
        });
        return result;
    }

    parseProgram(program, leds) {
        var self = this;
        var result = {
            maxEnd: -1,
        };
        program.split("\n").forEach(function (line) {
            line = line.trim();
            if (line.length == 0) {
                return;
            }
            var entry = {};
            var parts = line.split(";");
            for (var i = 0; i < parts.length; i++) {
                switch (i) {
                    case 0:
                        entry.ledId  = parseInt(parts[i]);
                        break;
                    case 1:
                        entry.begin = parseInt(parts[i]);
                        break;
                    case 2:
                        entry.end = parseInt(parts[i]);
                        if (entry.end > result.maxEnd) {
                            result.maxEnd = entry.end;
                        }
                        break;
                    case 3:
                        entry.function = parts[i];
                        break;
                    default:
                        if (!entry.params) {
                            entry.params = {};
                        }
                        entry.params[self.functions[entry.function].arguments[i - 4].name] = parts[i];
                }
            }
            // Are we refering to an undefined led here?
            if (!leds[entry.ledId]) {
                entry.ledId = 0;
            }
            if (!leds[entry.ledId].entries) {
                leds[entry.ledId].entries = [];
            }
            leds[entry.ledId].entries.push(entry);
        });
        return result;
    }

    activateProgram(index) {
        var self = this;
        fetch('/activate?id='+index).then((response) => self.fetchConfiguration());
    }

    clearAll() {
        var self = this;
        fetch('/clearall', {
          method: 'POST'
        }).then(() => self.fetchConfiguration());
    }

    getPersistableProgram(leds) {
        var result = "";
        var self = this;
        self.setState({ programError: undefined });
        if (leds) {
            leds.forEach((led, ledId) => {
                if (led.entries) {
                    led.entries.forEach((entry, entryId) => {
                        result += ledId;
                        result += ";";
                        result += entry.begin;
                        result += ";";
                        result += entry.end;
                        result += ";";
                        result += entry.function
                        if (entry.params) {
                            self.functions[entry.function].arguments.forEach((argument) => {
                                result += ";";
                                result += entry.params[argument.name];
                            });
                        }
                        result += "\n";
                    });
                }
            });
        }
        return result;
    }

    back() {
        var self = this;
        if (self.state.modified) {
            self.setState({
                showModifiedWarning: true
            })
        } else {
            // Go back immediately.
            self.goToList();
        }
    }

    goToList() {
        this.props.history.push('/');
    }

    save() {
        var self = this;
        var persistableProgram = this.getPersistableProgram(this.state.leds);
        if (persistableProgram == null) {
            return;
        }
        var body = "";
        body += persistableProgram;
        body += "-------\n";
        body += "NAME: " + this.state.name + "\n";
        body += "DESCRIPTION: " + this.state.description + "\n";
        this.state.leds.map((led) => {
            body += "LED: " + led.driver + ":" + led.address + ":" + led.channel + ";" + led.color + ";" + led.name + ";" + led.description + ";" + led.characteristic + "\n";
        });
        if (this.state.repetition != undefined) {
            body += "REPETITION: " + this.state.repetition + "\n";
        }
        if (this.state.scanner != undefined) {
            body += "SCANNER: " + this.state.scanner + "\n";
            if (this.state.scannerDarkLightPostDelay != undefined) {
                body += "SCANNER_DARK_LIGHT_POST_DELAY: " + this.state.scannerDarkLightPostDelay + "\n";
            }
        }
        //    console.log("Saving ", body);
        var url = '/program';
        //console.log("Body is\n ", body);
        var method = this.props.match.params.programId == undefined ? "POST" : "PUT";
        fetch(url, {
            method: method,
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
            body: "content="+body+(this.props.match.params.programId == undefined ? "" : "&id=" + this.props.match.params.programId)
        }).then((response) => response.text())
        .then((responseText) => {
            //this.props.history.push('/')
            self.fetchConfiguration();
            self.setState({modified: false, ledEdited: null});
        })
        .catch((error) => {
            console.error(error);
        });
    }

    newLED() {
        var led = {};
        led.driver = 0;
        led.address = 0;
        led.channel = 0;
        led.name = "(Name)";
        led.description = "(Description)";
        led.color = "#333333";
        led.characteristic = "Linear";
        this.setState((state) => { 
            state.leds.push(led); 
            state.ledEdited = state.leds.length-1;
            state.modified = true;
            return state; 
        });
    }

    newProgramEntry(ledId) {
        var entry = {};
        if (this.state.leds[ledId].entries && this.state.leds[ledId].entries.length > 0) {
            var lastEntry = this.state.leds[ledId].entries[this.state.leds[ledId].entries.length-1];
            entry.begin = lastEntry.end;
        } else {
            entry.begin = 0;
        }
        entry.end = entry.begin+1000;
        entry.function = "const";
        entry.params = {};
        entry.params["intensity"] = 50;
        this.setState((state) => {
            if (!state.leds[ledId].entries) {
                state.leds[ledId].entries = [];
            }
            state.leds[ledId].entries.push(entry);
            state.ledEdited = ledId;
            state.modified = true;
            return state;
        });
    }

    deleteProgramEntry(ledId, entryId) {
        this.setState((state) => {
            var removedEntry = this.state.leds[ledId].entries.splice(entryId, 1)[0];
            var removedDuration = removedEntry.end - removedEntry.begin;
            for (var i = entryId; i < this.state.leds[ledId].entries.length; i++) {
                this.state.leds[ledId].entries[i].begin -= removedDuration;
                this.state.leds[ledId].entries[i].end -= removedDuration;
            }
            state.modified = true;
            return state;
        });
    }

    moveProgramEntryUp(ledId, entryId) {
        if (entryId <= 0) {
            // Cannot move up.
            return;
        }
        this.setState((state) => {
            var previousEntry = this.state.leds[ledId].entries[entryId-1];
            var previousEntryBegin = previousEntry.begin;
            var previousEntryEnd = previousEntry.end;
            var entry = this.state.leds[ledId].entries[entryId];
            var entryBegin = entry.begin;
            var entryEnd = entry.end;
            this.state.leds[ledId].entries[entryId-1] = entry;
            this.state.leds[ledId].entries[entryId] = previousEntry;
            entry.begin = previousEntryBegin;
            entry.end = entry.begin + (entryEnd-entryBegin);
            previousEntry.begin = entry.end;
            previousEntry.end = previousEntry.begin + (previousEntryEnd-previousEntryBegin);
            state.modified = true;
            return state;
        });
    }

    moveProgramEntryDown(ledId, entryId) {
        if (entryId >= this.state.leds[ledId].entries.length-1) {
            // Cannot move down.
            return;
        }
        this.setState((state) => {
            var entry = this.state.leds[ledId].entries[entryId];
            var entryBegin = entry.begin;
            var entryEnd = entry.end;
            var nextEntry = this.state.leds[ledId].entries[entryId+1];
            var nextEntryBegin = nextEntry.begin;
            var nextEntryEnd = nextEntry.end;
            this.state.leds[ledId].entries[entryId] = nextEntry;
            this.state.leds[ledId].entries[entryId+1] = entry;
            nextEntry.begin = entryBegin;
            nextEntry.end = nextEntry.begin + (nextEntryEnd-nextEntryBegin);
            entry.begin = nextEntry.end;
            entry.end = entry.begin + (entryEnd-entryBegin);
            state.modified = true;
            return state;
        });
    }

    toggleColorPicker(id) {
        this.setState((state) => { 
            state.leds[id].displayColorPicker = state.ledEdited == id && !(state.leds[id].displayColorPicker || false); 
            return state 
        });
    }

    setLEDName(id, name) {
        this.setState((state) => { 
            state.leds[id].name = name; 
            state.modified = true;
            return state 
        });
    }

    setLEDColor(id, color) {
        this.setState((state) => { 
            state.leds[id].color = color; 
            state.modified = true;
            return state 
        });
    }

    setLEDDescription(id, description) {
        this.setState((state) => { 
            state.leds[id].description = description; 
            state.modified = true;
            return state 
        });
    }

    setLEDDriver(id, driverStr) {
        var driver = parseInt(driverStr);
        this.setState((state) => { 
            state.leds[id].driver = driver;
            state.modified = true;
            return state;
        });
        this.adjustAddressAndChannel();
    }

    setLEDCharacteristic(id, characteristic) {
        this.setState((state) => {
            state.leds[id].characteristic = characteristic;
            state.modified = true;
            return state;
        })
    }

    setLEDAddress(id, addressStr) {
        var address = parseInt(addressStr);
        this.setState((state) => { 
            state.leds[id].address = address;
            state.modified = true;
            return state 
        });
    }

    setLEDChannel(id, channelStr) {
        var channel = parseInt(channelStr);
        this.setState((state) => { 
            state.leds[id].channel = channel;
            state.modified = true;
            return state 
        });
    }

    setDuration(ledId, entryId, entry, durationStr) {
        var duration = parseInt(durationStr);
        if (isNaN(duration)) {
            duration = 0;
        }
        console.log("Duration is now ", duration);
        var change = (entry.end - entry.begin) - duration;
        entry.end = entry.end - change;
        this.setState((state) => {
            state.leds[ledId].entries[entryId] = entry;
            for (var i = entryId + 1; i < state.leds[ledId].entries.length; i++) {
                state.leds[ledId].entries[i].begin -= change;
                state.leds[ledId].entries[i].end -= change;
            }
            state.modified = true;
            return state;
        });
    }

    setFunction(ledId, entryId, entry, functionStr) {
        this.setState((state) => {
            state.leds[ledId].entries[entryId].function = functionStr;
            state.modified = true;
            return state;
        });
    }

    setArgument(ledId, entryId, argument, value) {
//console.log("Setting ", ledId, entryId, argument, value);
        this.setState((state) => {
            state.leds[ledId].entries[entryId].params[argument] = value;
            state.modified = true;
            return state;
        });
    }

    toggleLED(driver, address, channel) {
        var self = this;
        fetch('/toggle?driver='+ driver +'&address=' + address + '&channel=' + channel)
            .then((response) => response.text())
            .then((responseText) => {
                // Done
                self.fetchConfiguration();
            })
            .catch((error) => {
                console.error(error);
            });
    };

    renderTime(time) {
        return (
            <span>
                {time % 1000 == 0 ? time / 1000 + " s" : ((time - (time % 1000)) / 1000) + " s " + (time % 1000) + " ms"}
            </span>
        );
    }

    renderTiming = function (ledId, entryId, entry, edit) {
        if (edit) {
            return (
                <React.Fragment>
                    <td>
                        {this.renderTime(entry.begin)}&hellip;{this.renderTime(entry.end)}
                    </td>
                    <td>
                        <input type="number" className="form-control duration-input input-sm" value={entry.end - entry.begin} onChange={(e) => this.setDuration(ledId, entryId, entry, e.target.value)}></input> ms
                    </td>
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <td>{this.renderTime(entry.begin)}&hellip;{this.renderTime(entry.end)}</td>
                    <td>{this.renderTime(entry.end - entry.begin)}</td>
                </React.Fragment>
            );
        }
    }

    renderFunctionSelect = function (ledId, entryId, entry, edit) {
        if (edit) {
            return (
                <td>
                    <select className="form-control embedded-input input-sm" style={{ width: '10rem' }} value={entry.function} onChange={(e) => this.setFunction(ledId, entryId, entry, e.target.value)}>
                        {this.renderFunctionsAsOptions()}
                    </select>
                </td>
            );
        } else {
            return (
                <td>{entry.function}</td>
            );
        }
    }

    renderButtons(id, led, edit) {
        return (
            <div className="col-sm-3">
                <div className="pull-right">
                    {
                        this.state.ledEdited != id &&
                        <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.setState({ ledEdited: id })}>
                            <span className="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                        </button>
                    }
                    {
                        this.state.ledEdited == id &&
                        <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.setState({ ledEdited: undefined })}>
                            <span className="glyphicon glyphicon-ok"></span>&nbsp;Done
                        </button>
                    }
                    {
                        this.props.match.params.programId != this.state.active && this.state.ledStates.findIndex(el => el === led.driver+":"+led.address+":"+led.channel) == -1 &&
                        <button type="button" className="btn btn-default btn-xs btn-success" onClick={(e) => this.toggleLED(led.driver, led.address, led.channel)}>
                            <span className="glyphicon glyphicon-check"></span>&nbsp;Switch LED on
                        </button>
                    }
                    {
                        this.props.match.params.programId != this.state.active && this.state.ledStates.findIndex(el => el === led.driver+":"+led.address+":"+led.channel) != -1 &&
                        <button type="button" className="btn btn-default btn-xs btn-danger" onClick={(e) => this.toggleLED(led.driver, led.address, led.channel)}>
                            <span className="glyphicon glyphicon-unchecked"></span>&nbsp;Switch LED off
                        </button>
                    }
                    
                </div>
            </div>
        )
    }

    renderColorNameAndButtons(id, led, edit) {
        if (edit) {
            return (
                <div className="form-control-static">
                    <div className="col-sm-7">
                        <span className="swatch" onClick={(e) => { this.toggleColorPicker(id) }}>
                            <div className="color" style={{ background: led.color }}></div>
                        </span>
                        {
                            led.displayColorPicker &&
                            <span>
                                <div className="cover" onClick={(e) => { this.toggleColorPicker(id) }} />
                                <div className="popover" style={{ display: 'block' }} onClick={this.handleClose}>
                                    <SketchPicker disableAlpha={true} color={{ hex: led.color }} onChange={(color) => this.setLEDColor(id, color.hex)} />
                                </div>
                            </span>
                        }
                        &nbsp;
                    <input type="text" className="form-control embedded-input input-sm" value={led.name} onChange={(e) => this.setLEDName(id, e.target.value)}></input>
                    </div>
                    {this.renderButtons(id, led, edit)}
                </div>
            );
        } else {
            return (
                <div className="form-control-static">
                    <div className="col-sm-7">
                        <div className="swatch" onClick={(e) => { this.toggleColorPicker(id) }}>
                            <div className="color" style={{ background: led.color }}></div>
                        </div>&nbsp;
                    <span style={{ verticalAlign: 'top' }}>{led.name}</span>
                    </div>
                    {this.renderButtons(id, led, edit)}
                </div>
            );
        }
    }

    renderDescription(id, led, edit) {
        if (edit) {
            return (
                <div className="col-sm-10">
                    <textarea className="form-control" id="descriptionInput" placeholder="(Description)" value={led.description} onChange={(e) => this.setLEDDescription(id, e.target.value)}></textarea>
                </div>
            );
        } else {
            return (
                <div className="form-control-static">
                    <div className="col-sm-10">
                        {led.description}
                    </div>
                </div>
            );
        }
    }

    renderCharacteristic(id, led, edit) {
        if (edit) {
            var characteristicOptions = this.state.characteristics.map(function(item, index) {
                return (
                    <option key={'characteristics-option'+index} value={item}>{item}</option>
                );
            });

            return (
                <div className="col-sm-10">
                    <select className="form-control embedded-input input-sm" style={{ width: '10rem' }} value={led.characteristic} onChange={(e) => this.setLEDCharacteristic(id, e.target.value)}>
                        { characteristicOptions }
                    </select>    
                </div>
            );
        } else {
            return (
                <div className="form-control-static">
                    <div className="col-sm-10">
                        {led.characteristic}
                    </div>
                </div>
            );
        }
    }

    renderDriverAddressAndChannel(id, led, edit) {
        if (edit) {
            var driverOptions = this.state.drivers.map(function(item, index) {
                return (
                    <option key={'driver-option'+index} value={index}>{item.alias}</option>
                );
            });

            return (
                <div className="form-control-static">
                    <span className="col-sm-3">
                        Driver&nbsp;
                        <select className="form-control embedded-input input-sm" style={{ width: '10rem' }} value={led.driver} onChange={(e) => this.setLEDDriver(id, e.target.value)}>
                            { driverOptions }
                        </select>
                    </span>
                    <span className="col-sm-4">
                        Address&nbsp;
                        <select className="form-control embedded-input input-sm" style={{ width: '10rem' }} value={led.address} onChange={(e) => this.setLEDAddress(id, e.target.value)} disabled={this.state.drivers[led.driver].addressCount == 1}>
                            { this.renderNumbersAsOptions(this.state.drivers[led.driver].addressCount) }
                        </select>
                    </span>
                    <span className="col-sm-3">
                        Channel&nbsp;
                        <select className="form-control embedded-input input-sm" style={{ width: '10rem' }} value={led.channel} onChange={(e) => this.setLEDChannel(id, e.target.value)} disabled={this.state.drivers[led.driver].channelCount == 1}>
                            { this.renderNumbersAsOptions(this.state.drivers[led.driver].channelCount) }
                        </select>
                    </span>
                </div>
            );
        } else {
            return (
                <div className="form-control-static">
                    <div className="col-sm-10">
                        Driver <i>{this.state.drivers[led.driver].alias}</i> / Address <i>{led.address}</i> / Channel <i>{this.state.channelNames[led.channel]}</i>
                    </div>
                </div>
            );
        }
    }

    renderArgument(ledId, entryId, entry, argument, edit) {
//console.log("renderArgument", entry, argument, entry.params[argument.name]);
        if (edit) {
            return (
                <div key={ledId + "_" + entryId + "_" + argument.name}>
                    {argument.name}&nbsp;                        
                    <input type="number" min={argument.min} max={argument.max} className="form-control duration-input input-sm" value={entry.params[argument.name]} onChange={(e) => this.setArgument(ledId, entryId, argument.name, e.target.value)}></input>
                    &nbsp;{argument.unit != '1' ? argument.unit : ''}
                </div>
            );
        } else {
            return (
                <div key={ledId + "_" + entryId + "_" + argument.name}>{argument.name} {entry.params[argument.name]} {argument.unit != '1' ? argument.unit : ''}</div>
            );
        }
    }

    renderArguments(ledId, entryId, entry, edit) {
        return (
            <td>
                {this.functions[entry.function].arguments.map((argument) => this.renderArgument(ledId, entryId, entry, argument, edit))}
            </td>
        );
    }

    renderProgramEntry(ledId, entryId, entry, edit) {
        return (
            <tr key={ledId + entryId}>
                {this.renderTiming(ledId, entryId, entry, edit)}
                {this.renderFunctionSelect(ledId, entryId, entry, edit)}
                {this.renderArguments(ledId, entryId, entry, edit)}
                <td>
                    <span>
                        {
                            this.state.ledEdited == ledId && 
                            <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.deleteProgramEntry(ledId, entryId)}><span className="glyphicon glyphicon-remove"></span>&nbsp;Remove</button>
                        }
                        {
                            this.state.ledEdited == ledId && entryId > 0 &&
                            <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.moveProgramEntryUp(ledId, entryId)}><span className="glyphicon glyphicon-chevron-up"></span>&nbsp;Up</button>  
                        }
                        {
                            this.state.ledEdited == ledId && entryId < this.state.leds[ledId].entries.length - 1 &&
                            <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.moveProgramEntryDown(ledId, entryId)}><span className="glyphicon glyphicon-chevron-down"></span>&nbsp;Down</button>                   
                        }
                    </span>
                </td>
            </tr>
        );
    }

    renderProgram(id, led, edit) {
        return (
            <div className="form-control-static">
                <div className="col-sm-10">
                    <table className="table program-table">
                        <thead>
                            <tr>
                                <th className="time-column">Time</th>
                                <th className="duration-column">Duration</th>
                                <th className="function-column">Function</th>
                                <th className="arguments-column">Arguments</th>
                                <th className="operations-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {led.entries && led.entries.map((entry, entryIndex) => this.renderProgramEntry(id, entryIndex, entry, edit))}
                            {
                                this.state.ledEdited == id &&
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.newProgramEntry(id)}><span className="glyphicon glyphicon-plus"></span>&nbsp;New entry</button>
                                    </td>
                                </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    renderLED(id, led) {
        return (
            <span key={id}>
                <div className="form-group">
                    <label className="col-sm-2 control-label">LED {id}</label>
                    <div>
                        {this.renderColorNameAndButtons(id, led, this.state.ledEdited == id)}
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Description</label>
                    <div>
                        {this.renderDescription(id, led, this.state.ledEdited == id)}
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Characteristic</label>
                    {this.renderCharacteristic(id, led, this.state.ledEdited == id)}
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Connection</label>
                    {this.renderDriverAddressAndChannel(id, led, this.state.ledEdited == id)}
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label">Program</label>
                    {this.renderProgram(id, led, this.state.ledEdited == id)}
                </div>

                <hr />
            </span>
        )
    }

    renderFunctionAsOption(key, value) {
        return (
            <option key={key} value={key}>{key}</option>
        );
    }

    renderFunctionsAsOptions() {
        var self = this;
        return (
            <React.Fragment>
                {Object.keys(this.functions).map((key) => this.renderFunctionAsOption(key, this.functions[key]))}
            </React.Fragment>
        );
    }

    renderNumbersAsOptions(number) {
        var result = [];
        for (var i = 0; i < number; i++) {
            result.push((
                <option key={i} value={i}>{i}</option>
            ));
        }
        return result;
    }

    /**
     * This method makes sure that none of the configured LEDs makes use of a channel or address not allowed by the current driver.
     */
    adjustAddressAndChannel() {
        var self = this;
        if (self.state.drivers && self.state.leds) {
            self.state.leds.forEach((led, ledId) => {
                led.address = Math.min(led.address, self.state.drivers[led.driver].addressCount-1);
                led.channel = Math.min(led.channel, self.state.drivers[led.driver].channelCount-1);
            });
        }
    }

    render() {
        return (
            <div>
                <Modal show={this.state.showModifiedWarning} onHide={(e) => this.setState({ showModifiedWarning : false })} dialogClassName="custom-modal">
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">
                            Discard
                        </Modal.Title>
                    </Modal.Header>
                <Modal.Body>
                    Do you really want to discard the modified program? If not, please click Cancel and save your changes. 
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.goToList}>Discard</Button>
                    <Button onClick={(e) => this.setState({ showModifiedWarning : false })}>Cancel</Button>
                </Modal.Footer>
                </Modal>

                <form className="form-horizontal">
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button type="button" className="btn btn-default btn-xs" onClick={this.back}><span className="glyphicon glyphicon-chevron-left"></span>&nbsp;Back</button>
                            {
                                this.state.modified &&
                                <button type="button" className="btn btn-default btn-xs" onClick={this.save}><span className="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save</button>
                            }
                            {
                                !this.state.modified && this.props.match.params.programId != this.state.active &&
                                <button type="button" className="btn btn-default btn-xs btn-success" onClick={(e) => this.activateProgram(this.props.match.params.programId)}><span className="glyphicon glyphicon-check"></span>&nbsp;Activate</button>
                            }
                            {
                                !this.state.modified && this.props.match.params.programId == this.state.active &&
                                <button type="button" className="btn btn-default btn-xs btn-danger" onClick={(e) => this.clearAll(this.props.match.params.programId)}><span className="glyphicon glyphicon-unchecked"></span>&nbsp;Deactivate</button>
                            }
                        </div>
                    </div>

                    <hr />

                    <div className="form-group">
                        <label className="col-sm-2 control-label" htmlFor="nameInput">Name</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="nameInput" placeholder="Name" value={this.state.name} onChange={(e) => this.setState({ name: e.target.value, modified: true })}></input>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label" htmlFor="descriptionInput">Description</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="descriptionInput" placeholder="Program description" value={this.state.description} onChange={(e) => this.setState({ description: e.target.value, modified: true })}></input>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label" htmlFor="descriptionInput">Repetition</label>
                        <div className="col-sm-10">
                            <div className="radio">
                                <label>
                                    <input type="radio" name="repetitionOption1" checked={this.state.repetition == -1} onChange={(e) => this.setState({repetition: -1, modified: true})}></input>
                                    Unlimited
                                </label>
                            </div>
                            <div className="radio">
                                <label>
                                    <input type="radio" name="repetitionOption2" checked={this.state.repetition != -1} onChange={(e) => this.setState({repetition: 1, modified: true})}></input>
                                    Repeat&nbsp;
                                        {
                                            this.state.repetition > -1 &&
                                            <input type="number" value={this.state.repetition} onChange={(e) => this.setState({repetition: e.target.value, modified: true})} min="1"></input> 
                                        }
                                        {
                                            this.state.repetition == -1 && 
                                            <i>(n)</i>
                                        }
                                    &nbsp;
                                        {
                                            this.state.repetition == 1 ? "time" : "times"
                                        }
                                </label>
                            </div>
                        </div>
                    </div>

                    <hr />

                    {
                        this.state.leds.map((led, index) => this.renderLED(index, led))
                    }

                    {
                        //<div className="alert alert-danger">{this.state.programError}</div>
                    }

                    <div className="form-group">
                        <label className="col-sm-2 control-label">&nbsp;</label>
                        <div className="col-sm-10">
                            <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.newLED()}><span className="glyphicon glyphicon-plus"></span>&nbsp;New LED</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
export default Editor;
