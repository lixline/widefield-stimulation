import Editor from './Editor';
import React from 'react';
import { Link } from 'react-router-dom'
import './List.css';
import 'images/logo.png';

class List extends React.Component {
  constructor(props) {
    super(props);
    var self = this;
    this.state = {
      active: -1,
      programs: []
    };
    self.fetchConfiguration();
    fetch('/programs')
      .then((response) => response.text())
      .then((responseText) => {
        self.setState({programs: self.parseMetadata(responseText)});
      })
      .catch((error) => {
        console.error(error);
      });
  }

  fetchConfiguration() {
    var self = this;
    fetch('/configuration')
      .then((response) => response.json())
      .then((responseObj) => {
        self.setState({active: responseObj.active});
      })
      .catch((error) => {
        console.error(error);
      });
  }

  parseMetadata(responseText) {
    var result = [];
    var currentProgram;
    responseText.split("\n").forEach(function (line) {
      line = line.trim();
//      console.log("line ", line);
      if (line.startsWith("[") && line.endsWith("]")) {
        currentProgram = {};
        result.push(currentProgram);
      } else if (line.startsWith("NAME: ")) {
        currentProgram.name = line.substring("NAME: ".length);
      } else if (line.startsWith("DESCRIPTION: ")) {
        currentProgram.description = line.substring("DESCRIPTION: ".length);
      }
    });
    return result;
  }

  activateProgram(index) {
    var self = this;
    fetch('/activate?id='+index).then(() => self.fetchConfiguration());
  }

  clearAll() {
    var self = this;
    fetch('/clearall', {
      method: 'POST'
    }).then(() => self.fetchConfiguration());
  }

  newProgram() {
    this.props.history.push('/program');
  }

  renderIndex() {
    return (
      <table className="table table-hover">
        <thead key="header">
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>
              <button type="button" className="btn btn-default btn-xs" onClick={(e) => this.newProgram()}><span className="glyphicon glyphicon-plus"></span>&nbsp;New program</button>
              <button type="button" className="btn btn-default btn-xs btn-danger" onClick={(e) => this.clearAll()}><span className="glyphicon glyphicon-stop"></span>&nbsp;Deactivate All</button>
            </th>
          </tr>
        </thead>
        <tbody>       
          {
            this.state.programs.map((entry, index) => (
              <tr key={index}>
                <td>
                  {entry.name}
                </td>
                <td>
                  {entry.description}
                </td>
                <td>
                  {
                    this.state.active != index &&
                    <button type="button" className="btn btn-default btn-xs btn-success" onClick={(e) => this.activateProgram(index)}><span className="glyphicon glyphicon-check"></span>&nbsp;Activate</button>
                  }
                  {
                    this.state.active == index &&
                    <button type="button" className="btn btn-default btn-xs btn-danger" onClick={(e) => this.clearAll()}><span className="glyphicon glyphicon-unchecked"></span>&nbsp;Deactivate</button>
                  }
                  <Link to={{ pathname: '/program/'+index }}>
                    <button type="button" className="btn btn-default btn-xs"><span className="glyphicon glyphicon-pencil"></span>&nbsp;Edit</button>
                  </Link>
                  {
                    false &&
                    <button type="button" className="btn btn-default btn-xs"><span className="glyphicon glyphicon-remove"></span>&nbsp;Delete</button>
                  }
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    )
  }

  render() {
    return (
      <div>
        { this.renderIndex() }
      </div>
    );
  }
}
export default List;
