import './Editor.css';
import NumberFormat from 'react-number-format';
import React from 'react';

class Setup extends React.Component {

    stopFetching = [false];
    
    constructor(props) {
        super(props);
        this.state = {
            stopFetching: false,
            scannerConf: {}
        };

        //this.calculate = this.calculate.bind(this);
        //this.fetchConfiguration = this.fetchConfiguration.bind(self);
    }

    componentDidMount() { 
        this.fetchConfiguration(this);
    }

    componentWillUnmount() {
        console.log("Stopping fetcher");
        this.stopFetching = [true];
    }

    calculate(map) {
        if (map.darkCount) {
            map.darkCount = parseInt(map.darkCount);
        }
        if (map.lightCount) {
            map.lightCount = parseInt(map.lightCount);
        }
        if (map.darkTimeMicros) {
            map.darkTimeMicros = parseInt(map.darkTimeMicros);
        }
        if (map.lightTimeMicros) {
            map.lightTimeMicros = parseInt(map.lightTimeMicros);
        }
        if (map.darkCount && map.lightCount) {
            map.overallCount = map.darkCount + map.lightCount;
        }
        if (map.darkCount > 0) {
            map.averageDarkTimeMicros = map.darkTimeMicros/map.darkCount;
        }
        if (map.lightCount > 0) {
            map.averageLightTimeMicros = map.lightTimeMicros/map.lightCount;
        }
        if (map.overallCount > 0) {
            map.averageOverallTimeMicros = (map.darkTimeMicros+map.lightTimeMicros)/map.overallCount;
        }
        if (map.averageDarkTimeMicros > 0) {
            map.frequencyDarkkHz = 1000/map.averageDarkTimeMicros;
        }
        if (map.averageLightTimeMicros > 0) {
            map.frequencyLightkHz = 1000/map.averageLightTimeMicros;
        }
        if (map.averageOverallTimeMicros > 0) {
            map.frequencyOverallkHz = 1000/map.averageOverallTimeMicros;
        }
    }

    fetchConfiguration(self) {
        fetch('/scanner').then((response) => response.json())
        .then((scannerConf) => {

console.log("self", self.stopFetching);
//            this.calculate(scannerConf);
//            this.setState({ scannerConf: scannerConf });
            if (self.stopFetching && !self.stopFetching[0]) {
                setTimeout(() => self.fetchConfiguration(self), 500);
            }
        })
        .catch((error) => {
            console.error(error);
console.log("self", self.stopFetching);
            if (self.stopFetching && !self.stopFetching[0]) {
                setTimeout(() => self.fetchConfiguration(self), 500);
            }
        });
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-4"></div>
                    <div className="col-sm-2">Light</div>
                    <div className="col-sm-2">Dark</div>
                    <div className="col-sm-2">Overall</div>
                    <div className="col-sm-2">Unit</div>
                </div>
                <div className="row">
                    <div className="col-sm-4">Count</div>
                    <div className="col-sm-2">{this.state.scannerConf.lightCount}</div>
                    <div className="col-sm-2">{this.state.scannerConf.darkCount}</div>
                    <div className="col-sm-2">{this.state.scannerConf.overallCount}</div>
                    <div className="col-sm-2"></div>
                </div>
                <div className="row">
                    <div className="col-sm-4">Time</div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.lightTimeMicros} displayType="text" thousandSeparator={true}/></div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.darkTimeMicros} displayType="text" thousandSeparator={true}/></div>
                    <div className="col-sm-2"></div>
                    <div className="col-sm-2">µs</div>
                </div>
                <div className="row">
                    <div className="col-sm-4">Average Time</div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.averageLightTimeMicros} displayType="text" decimalScale={0} thousandSeparator={true}/></div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.averageDarkTimeMicros} displayType="text" decimalScale={0} thousandSeparator={true}/></div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.averageOverallTimeMicros} displayType="text" decimalScale={0} thousandSeparator={true}/></div>
                    <div className="col-sm-2">µs</div>
                </div>
                <div className="row">
                    <div className="col-sm-4">Frequency</div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.frequencyLightkHz} displayType="text" decimalScale={3} thousandSeparator={true}/></div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.frequencyDarkkHz} displayType="text" decimalScale={3} thousandSeparator={true}/></div>
                    <div className="col-sm-2"><NumberFormat value={this.state.scannerConf.frequencyOverallkHz} displayType="text" decimalScale={3} thousandSeparator={true}/></div>
                    <div className="col-sm-2">kHz</div>
                </div>
            </div>
        );
    }
}
export default Setup;
