import React from 'react';
import './App.css';
import { Link, Switch, Route, withRouter  } from 'react-router-dom'
import 'images/logo.png';
import List from './List'
import Editor from './Editor'
import Setup from './Setup'
import PropTypes from 'prop-types'

class App extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);
    var self = this;
    self.fetchConfiguration();
    setTimeout(e => {
      console.info("Timeout expired.");
      if (!self.state.bootstrapLoaded) {
        console.info("Enabled second bootstrap.");
        self.setState({bootstrapFailed: true});
      }
    }, 3000);
  }

  fetchConfiguration() {
    var self = this;
    self.state = {
      bootstrapLoaded: false,
      bootstrapFailed: false
    };
    fetch('/configuration')
      .then((response) => response.json())
      .then((responseObj) => {
        self.setState({wifiPower: responseObj.wifiPower});
      })
      .catch((error) => {
        console.error(error);
      });
  }

  bootstrapLoaded() {
    console.info("Bootstrap loaded.");
    this.setState({bootstrapLoaded: true});
  }

  render() {
    const { match, location, history } = this.props;
    return (
      <div>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" onLoad={e => this.bootstrapLoaded()}/>
        {
          this.state.bootstrapFailed && 
          <div>
            <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" onLoad={e => this.bootstrapLoaded()}/>
          </div>
        }
        <nav className="navbar navbar-default">
            <div className="navbar-header">
              <a className="navbar-brand" href="#">
                <img title={'Widefield Stimulation (Wifi Power: '+(this.state.wifiPower==='max'?'max':'-'+this.state.wifiPower+' dBm')+")"} src="images/logo.png" height="40px" ></img> 
              </a>
              &nbsp;
            </div>
            <div className="collapse navbar-collapse">
              <ul className="nav navbar-nav">
                <li className={location.pathname == '/setup' ? '' : 'active'}>
                <Link to={{ pathname: '/' }}>Programs</Link>
                </li>
                <li className={location.pathname == '/setup' ? 'active' : ''}>
                  <Link to={{ pathname: '/setup' }}>Setup</Link>
                </li>
              </ul>
            </div>
        </nav>
        <div className="container-fluid">
          <Switch>
            <Route exact path='/' component={List}/>
            <Route exact path='/program' component={Editor} activeMenu="programs"/>
            <Route path='/program/:programId' component={Editor} activeMenu="programs"/>
            <Route exact path="/setup" component={Setup} params="setup"/>
          </Switch>
        </div>
      </div>
    );
  }
}
export default withRouter(App);
