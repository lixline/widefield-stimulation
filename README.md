This project was initiated to enable the controlled light stimulation of (reasonably well transparent) biological samples on the stage of an inverted confocal laser scanning microscope (CLSM). Special features are:

1. The sample area stimulated can be adjusted.
2. The light stimulation is line synchronized with the CLSM image acquisition and can be performed simultaneously. Thus light stimulation is possible with any wavelength even if it spectrally overlaps with the detection range of the CLSM.
3. The stimulation pattern and brightness can be easily defined in a browser interface connected via WIFI to the microcontroller driving the stimulation LED.

In the following decription the technical details of the project are provided, do not hesitate to get in touch in case of any question!

# Hardware
## Electronic circuit

The basis for the hardware is an ESP32 microprocessor, here an Adafruit HUZZAH32 is used. It controls pairs of LEDs attached to MCP4922s (DAC - digital to analog converter) using the SPI bus of the ESP32. With the two MCP4922 shown in the [schematic](circuit-diagram-widefield-LED-stimulation.pdf), the setup can drive up to 4 LEDs in parallel, of which only one is shown in the schematic. For each LED type, a particular resistor needs to be included.

To be able to synchronize the light stimulation with the flyback time of the line-scanned laser of the CLSM, the LEDs are enabled/disabled in hardware because of the high speed required. The microscope is providing a 5V line trigger signal, this input is symbolized by the switch in the schematic. A logic gate (SN74LS37N) is used to convert (negate) the microscope line trigger signal in order to use it as the reference voltage (Vref) of the DACs. Thus, a high level input is converted into a zero reference voltage, turning of the LEDs.

## Mounting of stimulating LEDs

Within this project a thin tissue transparent enough to be successfully light stimulated from the opposite side than the confocal imaging is performed. For non transparent samples this method will not work.

An inverted CLSM is used meaning the objective lens is imaging the sample from below. The (Leica) halogen lamp house of the transmitted light arm has been modified by custom 3D printed parts allowing xyz centering of the LED on the microscope’s optical axis. Using the microscope's transmitted light arm the LED stimulation light is employed in Köhler illumination to stimulate the sample. The field diaphragm of the transmitted light path is employed to adjust the sample area stimulated. This selective illumination comes in handy because several measurements within the same sample are enabled.

In principle any stimulation wavelength can be used, the LED's spectral properties can be easily measured by a spectrometer. Here an Ocean optics USB4000 fiber spectrometer was used.

# Software

The software stack allows defining the brightness of the LEDs over time. For each LED, a series of functions can be defined that are used to calculate the LED brightness at any point in time after the program has been started. Programs can be configured to run a fixed number of times or to repeat forever. In addition, programs can be saved and reused.

Supported functions are *const* (for a constant brightness), *sina* (brightness changing from an initial to a maximum value following a sinus function with a constant frequency), *sinf* (brightness modulating between 0 and a maximum using a sinus function with a frequency changing over time), *staircase* (brightness changing from an initial to a final brightness changing in defined, constant steps) and *whitenoise* (a pseudo-randomized brightness).

For each LED, a type can be configured. The type is defined in the firmware and is subsequently known in the web UI as well. In the firmware, a characteristic for each LED type is stored that ensures that a configured LED brightness in the software is linearally mapped to the produced LED brightness. This is achieved by adjusting the voltage used to drive the LED appropriately.

The software contains of two components: a firmware for the ESP32 (written using the Arduino IDE and located in `src/main/huzzah`) and a React-base web UI (source located in `src/main/js`). The compiled web UI should be written to the [SPIFFs](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/spiffs.html) on the ESP32, allowing a user to access the UI from the ESP32s http endpoint `http://<arduino-ip>:80`.

## Customizing this solution

In order to access this project after being deployed to an ESP32, you need to configure (along with other settings) the WLAN access data that allows the ESP32 to obtain an IP and deliver its UI.

You can create a `config.properties` file in the `data` folder of this project, which can afterwards be uploaded to the SPIFFS on the ESP32 in order to customize this project. The content of the file is shown - with comments - below.

    # Replace <SSID> with the SSID of the wireless network you want your ESP32 to connect to.
    wifi.ssid=<SSID>

    # Replace <Key> with the WPA key of the wireless network you want your ESP32 to connect to.
    wifi.key=<Key>

    
## Open Issues

* Validation of function arguments (0 <= Intensity <= 255) etc.
* Start of first program entry can be changed (maybe, think about it one more time)
* Visualization of LED activity over time
* Drag and drop of program entries to change order (using https://github.com/react-dnd/react-dnd?)
* Dynamic adaption of the number of LEDs in Adafruit_NeoPixel, as this determines the loop frequence (one LED ca. 3kHz, 10 LEDs ca. 1,6kHz)
* Deletion of program steps and whole programs


## History

### Version 1.0.2

* Fix: deactivating the programs actually deactivates all LEDs
* Fix: repetition ends at correct point in time
* Fix: repetition setting correctly loaded in UI
* Fix: removed obsolete scanner settings from UI
* Fix: toggle reflects LED state even with multiple LEDs

# 